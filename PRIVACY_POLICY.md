# simplay MP3 player: Privacy policy

Welcome to simplay, a simple MP3-Player for Android!

This is an open source Android app, developed by Patrick Podobnig and Marcus Brisset. The source code is available on GitLab under the MIT license. The app itself is available on Google Play.

As avid Android users ourselves, we take privacy very seriously. We know how irritating it is when apps collect your data without your knowledge.

This app asks for permission to read the users phone storage. Furthermore it listens to the current phone state in order to pause playback on incoming phone calls.

We have not programmed this app to collect any personally identifiable information.

If you find any security vulnerability that has been inadvertently caused by us, or have any question regarding the app, please send us an e-mail and we most certainly will  find a way to fix the issure / help you.

Yours sincerely,

Patrick Podobnig, Marcus Brisset  
marcus.c.brisset@gmail.com