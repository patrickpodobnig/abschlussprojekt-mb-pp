# Final project @ CODERS.BAY Vienna: simplay - MP3 Player for Android  
by Patrick Podobnig && Marcus Brisset

## Stack
Android Studio (Java) && SQLite

#### Assets
Macromedia Fireworks MX 2004, InkScape  
<i>all assets made by ourselves</i>

## Download (v1.03)
[Google Play Store](https://play.google.com/store/apps/details?id=com.finalproject.simplay)

#### Known issues
Crashes when trying to play unsupported media. ([Supported formats](https://developer.android.com/guide/topics/media/media-formats))