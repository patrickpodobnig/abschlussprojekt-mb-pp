package com.finalproject.simplay.model;

public class AudioFileExtras {

	private String album;
	private String year;
	private String genre;
	private int dateAdded;
	private String fileName;
	private long size;

	public AudioFileExtras(String album, String year, String genre, int dateAdded, String fileName, long size) {
		this.album = album;
		this.year = year;
		this.genre = genre;
		this.dateAdded = dateAdded;
		this.fileName = fileName;
		this.size = size;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(int dateAdded) {
		this.dateAdded = dateAdded;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

}
