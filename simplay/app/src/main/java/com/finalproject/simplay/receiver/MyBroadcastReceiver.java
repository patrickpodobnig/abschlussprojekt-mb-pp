package com.finalproject.simplay.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import com.finalproject.simplay.service.MusicService;
import com.finalproject.simplay.util.Helper;

public class MyBroadcastReceiver extends BroadcastReceiver {

	private static final String DEBUG = Helper.setDebugTag("MyBroadcastReceiver");

	public MyBroadcastReceiver() {
		super();
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent serviceIntent = new Intent(context, MusicService.class);
		switch (intent.getAction()) {

			case AudioManager.ACTION_AUDIO_BECOMING_NOISY:
				Log.d(DEBUG, "Bluetooth media device disconnected ...");

				serviceIntent.setAction(MusicService.ACTION_OUTPUT_DEVICE_CHANGED);
				context.startService(serviceIntent);
				break;

			case Intent.ACTION_MEDIA_BUTTON:

				final KeyEvent event = (KeyEvent) intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
				// We always receive ACTION_DOWN followed BY ACTION_UP, everything would be invoked twice.
				if (event.getAction() != KeyEvent.ACTION_DOWN) {
					return;
				}
				Log.d(DEBUG, "Media button command received ...");

				switch (event.getKeyCode()) {

					case KeyEvent.KEYCODE_MEDIA_STOP:
						Toast.makeText(context, "STOP", Toast.LENGTH_SHORT).show();
						break;

					case KeyEvent.KEYCODE_HEADSETHOOK:
					case KeyEvent.KEYCODE_MEDIA_PLAY:
					case KeyEvent.KEYCODE_MEDIA_PAUSE:
					case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
						serviceIntent.setAction(MusicService.ACTION_PAUSE);
						break;

					case KeyEvent.KEYCODE_MEDIA_NEXT:
						serviceIntent.setAction(MusicService.ACTION_NEXT);
						break;

					case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
						serviceIntent.setAction(MusicService.ACTION_PREV);
						break;
				}
				context.startService(serviceIntent);
				break;
		}
	}

}