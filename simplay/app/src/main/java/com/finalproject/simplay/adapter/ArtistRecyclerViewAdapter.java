package com.finalproject.simplay.adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.simplay.R;
import com.finalproject.simplay.util.Helper;
import com.turingtechnologies.materialscrollbar.INameableAdapter;

import java.util.ArrayList;

public class ArtistRecyclerViewAdapter extends RecyclerView.Adapter<ArtistRecyclerViewAdapter.ArtistViewHolder>
implements INameableAdapter {

    private ArrayList<String> artistList;

    public void setArtistList(ArrayList<String> artistList) {
        this.artistList = artistList;
    }

    @NonNull
    @Override
    public ArtistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_generic, parent, false);
        return new ArtistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ArtistViewHolder holder, int position) {
        holder.artistName.setText(Helper.clampString(artistList.get(position), Helper.STRLEN_LIST_ITEM_GENERIC));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("filterString", holder.artistName.getText().toString());
                bundle.putString("origin", "Artist");
                Navigation.findNavController(view).navigate(R.id.action_viewPagerFragmentContainer_to_specificSongs, bundle);
            }
        });
    }

    @Override
    public int getItemCount() {
        return artistList.size();
    }

    @Override
    public Character getCharacterForElement(int element) {
        char c = artistList.get(element).charAt(0);
        if (Character.isDigit(c)) {
            c = '#';
        }
        return c;
    }

    public static class ArtistViewHolder extends RecyclerView.ViewHolder {
        private final TextView artistName;

        public ArtistViewHolder(@NonNull View itemView) {
            super(itemView);
            artistName = itemView.findViewById(R.id.txtGenericList);
        }
    }
}
