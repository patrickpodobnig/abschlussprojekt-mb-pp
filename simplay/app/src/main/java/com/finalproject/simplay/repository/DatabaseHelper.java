package com.finalproject.simplay.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.finalproject.simplay.configuration.PlayMode;
import com.finalproject.simplay.model.AudioFile;
import com.finalproject.simplay.model.Playlist;
import com.finalproject.simplay.model.Bookmark;
import com.finalproject.simplay.util.Formatter;
import com.finalproject.simplay.util.MyBuilder;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static DatabaseHelper instance;
    private final Context calledByContext;

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "simplay.db";

    public static final String PLAYBACK_SETTINGS_TABLE = "playback_settings";
    public static final String PLAYBACK_MODE = "mode";
    public static final String PLAYBACK_VALUE = "value";
    public static final String SNAPSHOT_PLAYLIST_TABLE = "snapshot_playlist";
    public static final String SNAPSHOT_PLAYBACK_TABLE = "snapshot_playback";
    public static final String SNAPSHOT_PLAYLIST_POS = "playlist_pos";
    public static final String SNAPSHOT_PLAYBACK_POS = "playback_pos";
    public static final String PLAYLIST_TABLE = "playlist_table";
    public static final String PLAYLIST_TABLE_NAME = "playlist_table_name";
    public static final String PLAYLIST_TITLE = "playlist_title";
    public static final String PERSONAL_PLAYLIST_PREFIX = "personal_playlist_";
    public static final String INDEX = "auto_id";
    public static final String ID = "id";
    public static final String ARTIST = "artist";
    public static final String TITLE = "title";
    public static final String DURATION = "duration";
    public static final String DATA_PATH = "dataPath";
    public static final String BOOKMARK_TABLE = "bookmark_table";
    public static final String BOOKMARK_ID = "bookmark_id";
    public static final String SONG_ID = "song_id";
    public static final String POSITION = "position";



    /***************************************************
     * DATABASE SCHEMAS
     ***************************************************/

    public static final String SNAPSHOT_PLAYLIST_TABLE_SCHEMA =
            "CREATE TABLE " + SNAPSHOT_PLAYLIST_TABLE + "(" +
                    INDEX + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    ID + " INTEGER NOT NULL, " +
                    ARTIST + " VARCHAR(50) NOT NULL, " +
                    TITLE + " VARCHAR(100) NOT NULL, " +
                    DURATION + " INTEGER NOT NULL, " +
                    DATA_PATH + " VARCHAR(100) NOT NULL)";

    public static final String SNAPSHOT_PLAYBACK_TABLE_SCHEMA =
            "CREATE TABLE " + SNAPSHOT_PLAYBACK_TABLE + " (" +
                    "type INTEGER NOT NULL, " +
                    "value LONG NOT NULL)";

    public static final String PLAYLIST_TABLE_SCHEMA =
            "CREATE TABLE " + PLAYLIST_TABLE + " (" +
                    ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    PLAYLIST_TABLE_NAME + " VARCHAR(50) NOT NULL, " +
                    PLAYLIST_TITLE + " VARCHAR(50) NOT NULL)";

    public static final String BOOKMARK_TABLE_SCHEMA =
            "CREATE TABLE " + BOOKMARK_TABLE + " (" +
                    BOOKMARK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    SONG_ID + " INTEGER NOT NULL, " +
                    POSITION + " INTEGER NOT NULL, " +
                    TITLE + " VARCHAR(50) NOT NULL)";

    public static final String PLAYBACK_SETTINGS_TABLE_SCHEMA =
            "CREATE TABLE " + PLAYBACK_SETTINGS_TABLE + " (" +
                    PLAYBACK_MODE + " VARCHAR(50) PRIMARY KEY, " +
                    PLAYBACK_VALUE + " INTEGER NOT NULL)";

    public static final String SNAPSHOT_PLAYBACK_SCHEMA =
            "INSERT INTO " + SNAPSHOT_PLAYBACK_TABLE + " VALUES " +
                    "('" + SNAPSHOT_PLAYLIST_POS + "', -1), " +
                    "('" + SNAPSHOT_PLAYBACK_POS + "', -1)";

    public static final String PLAYBACK_SETTINGS_SCHEMA =
            "INSERT INTO " + PLAYBACK_SETTINGS_TABLE + " VALUES " +
                    "('" + PlayMode.MODE_SHUFFLE + "', '" + PlayMode.MODE_INACTIVE + "'), " +
                    "('" + PlayMode.MODE_REPEAT + "', '" + PlayMode.MODE_INACTIVE + "')";

    public static DatabaseHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseHelper(context);
        }
        return instance;
    }

    private DatabaseHelper(Context context) {
        super(context.getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);
        this.calledByContext = context.getApplicationContext();
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(PLAYLIST_TABLE_SCHEMA);
        database.execSQL(BOOKMARK_TABLE_SCHEMA);
        database.execSQL(PLAYBACK_SETTINGS_TABLE_SCHEMA);
        database.execSQL(SNAPSHOT_PLAYLIST_TABLE_SCHEMA);
        database.execSQL(SNAPSHOT_PLAYBACK_TABLE_SCHEMA);
        database.execSQL(PLAYBACK_SETTINGS_SCHEMA);
        database.execSQL(SNAPSHOT_PLAYBACK_SCHEMA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int i, int i1) {
        database.execSQL("DROP TABLE IF EXISTS " + PLAYLIST_TABLE);
        database.execSQL("DROP TABLE IF EXISTS " + BOOKMARK_TABLE);
        database.execSQL("DROP TABLE IF EXISTS " + PLAYBACK_SETTINGS_TABLE);
        database.execSQL("DROP TABLE IF EXISTS " + SNAPSHOT_PLAYLIST_TABLE);
        database.execSQL("DROP TABLE IF EXISTS " + SNAPSHOT_PLAYBACK_TABLE);
        onCreate(database);
    }



    /***************************************************
     * BOOKMARKS
     ***************************************************/

    public ArrayList<Bookmark> getBookmarkList(int songId) {
        ArrayList<Bookmark> bookmarkList = new ArrayList<>();
        SQLiteDatabase database = getReadableDatabase();

        try (
                Cursor cursor = database.rawQuery(
                        "SELECT * FROM " + BOOKMARK_TABLE +
                                " WHERE " + SONG_ID + "=" + songId +
                                " ORDER BY " + POSITION, null)
        ) {
            int bookmarkIdCol = cursor.getColumnIndexOrThrow(BOOKMARK_ID);
            int positionCol = cursor.getColumnIndexOrThrow(POSITION);
            int titleCol = cursor.getColumnIndexOrThrow(TITLE);
            while (cursor.moveToNext()) {
                int bookmarkId = cursor.getInt(bookmarkIdCol);
                int position = cursor.getInt(positionCol);
                String title = cursor.getString(titleCol);
                bookmarkList.add(new Bookmark(bookmarkId, songId, position, title));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        database.close();
        return bookmarkList;
    }

    public void addBookmark(Bookmark bookmark) {
        SQLiteDatabase database = getWritableDatabase();

        String valueString = "(" +
                "NULL, " +
                bookmark.getSongId() + ", " +
                bookmark.getPosition() + ", " +
                Formatter.prepareDblQuotedString(bookmark.getTitle()) +
                ")";

        database.execSQL("INSERT INTO " + BOOKMARK_TABLE + " VALUES " + valueString);
        database.close();
        MyBuilder.createToast(calledByContext, MyBuilder.TOAST_SYSTEM, "Bookmark saved.").show();
    }

    public void deleteBookmark(int id) {
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL("DELETE FROM " + BOOKMARK_TABLE + " WHERE " + BOOKMARK_ID + "=" + id);
        database.close();
        MyBuilder.createToast(calledByContext, MyBuilder.TOAST_NOTIFICATION, "Bookmark deleted.").show();
    }



    /***************************************************
     * PLAYLISTS
     ***************************************************/

    public void createNewPlaylist(String playlistTitle) {
        SQLiteDatabase database = getWritableDatabase();
        String newPlaylistTableName = PERSONAL_PLAYLIST_PREFIX + getNextPlaylistID();

        database.execSQL(
                "CREATE TABLE " + newPlaylistTableName + "("
                        + INDEX + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        ID + " INTEGER NOT NULL, " +
                        ARTIST + " VARCHAR(50) NOT NULL, " +
                        TITLE + " VARCHAR(100) NOT NULL, " +
                        DURATION + " INTEGER NOT NULL, " +
                        DATA_PATH + " VARCHAR(100) NOT NULL)"
        );
        database.execSQL(
                "INSERT INTO " + PLAYLIST_TABLE + " VALUES" +
                        "(NULL, " +
                        Formatter.prepareDblQuotedString(newPlaylistTableName) + ", " +
                        Formatter.prepareDblQuotedString(playlistTitle) +
                        ")"
        );
        database.close();
        MyBuilder.createToast(calledByContext, MyBuilder.TOAST_SYSTEM, "Successfully created playlist.").show();
    }

    public void deletePlaylist(String playlistTableName) {
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL("DROP TABLE " + playlistTableName);
        database.execSQL("DELETE FROM " + PLAYLIST_TABLE +
                " WHERE " + PLAYLIST_TABLE_NAME + " = " + Formatter.prepareDblQuotedString(playlistTableName));
        database.close();
        MyBuilder.createToast(calledByContext, MyBuilder.TOAST_SYSTEM, "Successfully deleted playlist.").show();
    }

    public void renamePlaylist(String playlistTableName, String playlistTitle) {
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL("UPDATE " + PLAYLIST_TABLE +
                " SET " + PLAYLIST_TITLE + " = " + Formatter.prepareDblQuotedString(playlistTitle) +
                " WHERE " + PLAYLIST_TABLE_NAME + " = " + Formatter.prepareDblQuotedString(playlistTableName));
        database.close();
        MyBuilder.createToast(calledByContext, MyBuilder.TOAST_SYSTEM, "Successfully renamed playlist.").show();
    }

    public void addToPlaylist(String playlistTableName, ArrayList<AudioFile> newList) {
        if (newList.size() == 0) {
            return;
        }

        StringBuilder valueString = new StringBuilder();
        for (int i = 0; i < newList.size(); i++) {
            AudioFile audioFile = newList.get(i);
            if (i > 0) {
                valueString.append(", ");
            }
            valueString.append("(NULL, ")
                    .append(audioFile.getId()).append(", ")
                    .append(Formatter.prepareDblQuotedString(audioFile.getArtist())).append(", ")
                    .append(Formatter.prepareDblQuotedString(audioFile.getTitle())).append(", ")
                    .append(audioFile.getDuration()).append(", ")
                    .append(Formatter.prepareDblQuotedString(audioFile.getDataPath())).append(")");
        }

        SQLiteDatabase database = getWritableDatabase();
        database.execSQL("INSERT INTO " + playlistTableName + " VALUES " + valueString);
        database.close();
        MyBuilder.createToast(calledByContext, MyBuilder.TOAST_SYSTEM, "Successfully added songs to playlist.").show();
    }

    public void addToPlaylist(String playlistName, AudioFile audioFile) {
        SQLiteDatabase database = getWritableDatabase();

        String valueString = "(NULL, " +
                audioFile.getId() + ", " +
                Formatter.prepareDblQuotedString(audioFile.getArtist()) + ", " +
                Formatter.prepareDblQuotedString(audioFile.getTitle()) + ", " +
                audioFile.getDuration() + ", " +
                Formatter.prepareDblQuotedString(audioFile.getDataPath()) + ")";

        database.execSQL("INSERT INTO " + playlistName + " VALUES " + valueString);
        database.close();
        //MyBuilder.createToast(calledByContext, MyBuilder.TOAST_SYSTEM, "Successfully added song playlist.").show();
    }

    public void updatePlaylist(String playlistTableName, ArrayList<AudioFile> newList) {
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL("DELETE FROM " + playlistTableName);

        StringBuilder valueString = new StringBuilder();
        for (int i = 0; i < newList.size(); i++) {
            AudioFile audioFile = newList.get(i);
            if (i > 0) {
                valueString.append(", ");
            }
            valueString.append("(NULL, ")
                    .append(audioFile.getId()).append(", ")
                    .append(Formatter.prepareDblQuotedString(audioFile.getArtist())).append(", ")
                    .append(Formatter.prepareDblQuotedString(audioFile.getTitle())).append(", ")
                    .append(audioFile.getDuration()).append(", ")
                    .append(Formatter.prepareDblQuotedString(audioFile.getDataPath())).append(")");
        }

        database.execSQL("INSERT INTO " + playlistTableName + " VALUES " + valueString);
        database.close();
        MyBuilder.createToast(calledByContext, MyBuilder.TOAST_SYSTEM, "Successfully updated playlist.").show();
    }

    public void deleteFromPlaylist(String playlistTableName, ArrayList<AudioFile> songList) {
        if (songList.size() == 0) {
            return;
        }

        StringBuilder valueString = new StringBuilder();
        for (int i = 0; i < songList.size(); i++) {
            if (i > 0) {
                valueString.append(" OR ");
            }
            valueString.append(INDEX + " = ").append(songList.get(i).getDbIndex());
        }

        SQLiteDatabase database = getWritableDatabase();
        database.execSQL("DELETE FROM " + playlistTableName + " WHERE " + valueString);
        database.close();
        MyBuilder.createToast(calledByContext, MyBuilder.TOAST_SYSTEM, "Successfully deleted songs from playlist.").show();
    }

    public void deleteFromPlaylist(String playlistTableName, AudioFile audioFile) {
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL("DELETE FROM " + playlistTableName + " WHERE " + INDEX + " = " + audioFile.getDbIndex());
        database.close();
        MyBuilder.createToast(calledByContext, MyBuilder.TOAST_SYSTEM, "Successfully deleted song from playlist.").show();
    }

    public ArrayList<Playlist> getPlayLists() {
        ArrayList<Playlist> nameList = new ArrayList<>();
        SQLiteDatabase database = getReadableDatabase();

        try (Cursor cursor = database.rawQuery("SELECT * FROM " + PLAYLIST_TABLE, null)) {
            int tableNameCol = cursor.getColumnIndexOrThrow(PLAYLIST_TABLE_NAME);
            int titleCol = cursor.getColumnIndexOrThrow(PLAYLIST_TITLE);
            while (cursor.moveToNext()) {
                String tableName = cursor.getString(tableNameCol);
                String title = cursor.getString(titleCol);
                nameList.add(new Playlist(tableName, title));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        database.close();
        return nameList;
    }

    public ArrayList<AudioFile> getPlaylist(String playlistTableName) {
        ArrayList<AudioFile> songList = new ArrayList<>();
        SQLiteDatabase database = getReadableDatabase();

        try (Cursor cursor = database.rawQuery("SELECT * FROM " + playlistTableName, null)) {
            int indexCol = cursor.getColumnIndexOrThrow(INDEX);
            int idCol = cursor.getColumnIndexOrThrow(ID);
            int artistCol = cursor.getColumnIndexOrThrow(ARTIST);
            int titleCol = cursor.getColumnIndexOrThrow(TITLE);
            int durationCol = cursor.getColumnIndexOrThrow(DURATION);
            int dataCol = cursor.getColumnIndexOrThrow(DATA_PATH);

            while (cursor.moveToNext()) {
                int index = cursor.getInt(indexCol);
                int id = cursor.getInt(idCol);
                String artist = cursor.getString(artistCol);
                String title = cursor.getString(titleCol);
                int duration = cursor.getInt(durationCol);
                String data = cursor.getString(dataCol);
                songList.add(new AudioFile(index, id, artist, title, duration, data));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        database.close();
        return songList;
    }

    public String getPlaylistName(String playlistTableName) {
        String playlistName = "";
        SQLiteDatabase database = getReadableDatabase();
        try (Cursor cursor = database.rawQuery("SELECT " + PLAYLIST_TITLE + " FROM " + PLAYLIST_TABLE + " WHERE " + PLAYLIST_TABLE_NAME + " = " + Formatter.prepareDblQuotedString(playlistTableName), null)) {
            int titleCol = cursor.getColumnIndexOrThrow(PLAYLIST_TITLE);
            if (cursor.moveToFirst()) {
                playlistName = cursor.getString(titleCol);
            }
        }
        return playlistName;
    }

    public int getNextPlaylistID() {
        int nextID = 1;
        SQLiteDatabase database = getReadableDatabase();

        try (
                Cursor cursor = database.rawQuery(
                    "SELECT MAX(" + ID + ") FROM " + PLAYLIST_TABLE + " LIMIT 1",
                    null)
            ) {
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    nextID = cursor.getInt(0);
                }
                return nextID + 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return nextID;
    }



    /***************************************************
     * PLAYBACK SNAPSHOT
     ***************************************************/

    public void takeSnapshot(ArrayList<AudioFile> currentPlaylist, int currentPlaylistPos, int currentPlaybackPos) {
        SQLiteDatabase database = getWritableDatabase();

        database.execSQL("DELETE FROM " + SNAPSHOT_PLAYLIST_TABLE);

        StringBuilder dumpSnippet = new StringBuilder();
        for (int i = 0; i < currentPlaylist.size(); i++) {
            AudioFile audioFile = currentPlaylist.get(i);
            if (i > 0) {
                dumpSnippet.append(", ");
            }
            dumpSnippet.append("(NULL, ")
                    .append(audioFile.getId()).append(", ")
                    .append(Formatter.prepareDblQuotedString(audioFile.getArtist())).append(", ")
                    .append(Formatter.prepareDblQuotedString(audioFile.getTitle())).append(", ")
                    .append(audioFile.getDuration()).append(", ")
                    .append(Formatter.prepareDblQuotedString(audioFile.getDataPath())).append(")");
        }
        database.execSQL("INSERT INTO " + SNAPSHOT_PLAYLIST_TABLE + " VALUES " + dumpSnippet);
        database.execSQL(
                "UPDATE " + SNAPSHOT_PLAYBACK_TABLE +
                        " SET value = " + currentPlaylistPos +
                        " WHERE type = " + Formatter.prepareDblQuotedString(SNAPSHOT_PLAYLIST_POS)
        );
        database.execSQL(
                "UPDATE " + SNAPSHOT_PLAYBACK_TABLE +
                        " SET value = " + currentPlaybackPos +
                        " WHERE type = " + Formatter.prepareDblQuotedString(SNAPSHOT_PLAYBACK_POS)
        );
        database.close();
    }

    public ArrayList<AudioFile> retrievePlaylistSnapshot() {
        ArrayList<AudioFile> playlistSnapshot = getPlaylist(SNAPSHOT_PLAYLIST_TABLE);
        if (playlistSnapshot.size() == 0) {
            return null;
        }
        return playlistSnapshot;
    }

    public ContentValues retrievePlaybackSnapshot() {
        return getContentValues(SNAPSHOT_PLAYBACK_TABLE);
    }



    /***************************************************
     * PLAYBACK SETTINGS
     ***************************************************/

    public ContentValues getContentValues(String tableName) {
        ContentValues settings = new ContentValues();
        SQLiteDatabase database = getReadableDatabase();

        try (Cursor cursor = database.rawQuery("SELECT * FROM " + tableName, null)) {
            while (cursor.moveToNext()) {
                settings.put(cursor.getString(0), cursor.getString(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        database.close();
        return settings;
    }

    public void setShuffleMode(String mode) {
        SQLiteDatabase database = getWritableDatabase();

        database.execSQL(
                "UPDATE " + PLAYBACK_SETTINGS_TABLE +
                        " SET " + PLAYBACK_VALUE + " = " + Formatter.prepareDblQuotedString(mode) +
                        " WHERE " + PLAYBACK_MODE + " = " + Formatter.prepareDblQuotedString(PlayMode.MODE_SHUFFLE)
        );

        database.close();
    }

    public void setRepeatMode(String mode) {
        SQLiteDatabase database = getWritableDatabase();

        database.execSQL(
                "UPDATE " + PLAYBACK_SETTINGS_TABLE +
                        " SET " + PLAYBACK_VALUE + " = " + Formatter.prepareDblQuotedString(mode) +
                        " WHERE " + PLAYBACK_MODE + " = " + Formatter.prepareDblQuotedString(PlayMode.MODE_REPEAT)
        );

        database.close();
    }
}
