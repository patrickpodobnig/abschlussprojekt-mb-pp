package com.finalproject.simplay;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.simplay.adapter.BookmarkRecyclerViewAdapter;
import com.finalproject.simplay.model.AudioFile;
import com.finalproject.simplay.model.AudioFileExtras;
import com.finalproject.simplay.model.Bookmark;
import com.finalproject.simplay.repository.MediaStoreHelper;
import com.finalproject.simplay.service.MusicService;
import com.finalproject.simplay.repository.DatabaseHelper;
import com.finalproject.simplay.util.Formatter;
import com.finalproject.simplay.util.Helper;
import com.finalproject.simplay.configuration.PlayMode;
import com.finalproject.simplay.util.MyBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class PlayerActivity extends AppCompatActivity {

	private static final String DEBUG = Helper.setDebugTag("PlayerActivity");

	// [!WeakReference] Thanks to stackoverflow, we now can call our playSelectedPosition() method from within our adapter class
	private static WeakReference<PlayerActivity> weakActivity;


    private AnimatorSet frontAnim, backAnim;
    private boolean isFront = true;
    private RecyclerView bookMarkRecView;
    private ConstraintLayout bookMarkSection, albumArtContainer;
    private FloatingActionButton fabAddBM, fabDeleteBM, fabTurnToFront, fabTurnToBack;
    private final BookmarkRecyclerViewAdapter bookmarkAdapter = new BookmarkRecyclerViewAdapter();
    private ArrayList<Bookmark> bookmarkList = new ArrayList<>();

	private ImageButton btnPlayPause, btnPrevious, btnNext, btnShuffle, btnRepeat;
	private TextView txtCurrentArtist, txtCurrentTitle, txtDurationCurrent, txtDurationTotal;
	private ImageView ivAlbumArt;
	private MusicService musicService;
	private boolean musicServiceBound = false;
	private DatabaseHelper dbHelper;
	private ContentValues playbackSettings;

	private SeekBar audioFileProgressBar;
	private boolean audioFileProgressBarTouched = false;


	/***************************************************
	 Handlers, Listeners, ...
	 ***************************************************/

	private final Handler handler = new Handler();

	private final Runnable updateProgressBar = new Runnable() {
		@Override
		public void run() {
			if (!audioFileProgressBarTouched && musicService.getCurrentAudioFile() != null) {
				audioFileProgressBar.setProgress(musicService.getCurrentPosition());
				txtDurationCurrent.setText(Formatter.msToMinutesAndSeconds(musicService.getCurrentPosition(), musicService.getDuration()));
			}
			handler.postDelayed(this, 100);
		}
	};

	private final ServiceConnection musicServiceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
			MusicService.MusicBinder musicBinder = (MusicService.MusicBinder) iBinder;
			musicService = musicBinder.getService();
			musicServiceBound = true;
			Log.d(DEBUG, "Service connected ...");

			if (musicService.getCurrentAudioFile() != null) {
				updateCurrentAudioFile(musicService.getCurrentAudioFile());
				bookmarkAdapter.setSongLengthInMs(musicService.getCurrentAudioFile().getDuration());
			}
			updateBtnPlayPauseState();
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			musicServiceBound = false;
			Log.d(DEBUG, "Service disconnected ...");
		}
	};

	private final SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {

		// Declare local variable to pass position around
		private int position;

		@Override
		public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
			this.position = i;
			// Update current time TextView when moving the slider
			if (musicServiceBound) {
				txtDurationCurrent.setText(Formatter.msToMinutesAndSeconds(i, musicService.getDuration()));
			}
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			audioFileProgressBarTouched = true;
		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			audioFileProgressBarTouched = false;
			musicService.seekTo(position);
		}
	};

	// [!WeakReference] Static method to provide a weak reference to this class
	public static PlayerActivity getInstanceActivity() {
		if (weakActivity != null) {
			return weakActivity.get();
		}
		return null;
	}

	// [ACTIONBAR]
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_player_activity, menu);
		return true;
	}

	@SuppressLint("NonConstantResourceId")
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				return true;
			case R.id.miCurrentPlaylist:
				startActivity(new Intent(this, CurrentPlaylistActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}



	/***************************************************
	 Activity Overrides
	 ***************************************************/

	@Override
	protected void onStart() {
		super.onStart();
		Intent musicServiceIntent = new Intent(this, MusicService.class);
		bindService(musicServiceIntent, musicServiceConnection, BIND_AUTO_CREATE);

		// TODO: Monitor behaviour, needed this fix in case of navigating back to a previous player view.
		//  This can happen if one presses the home button in player view, then uses the notification to get back.
		//  "History" would be: PlayerActivity < CategoryActivity < PlayerActivity (first PA wasn't updating)
		if (musicServiceBound) {
			updateCurrentAudioFile(musicService.getCurrentAudioFile());
		}
		updateBtnPlayModeState();
		updateBtnPlayPauseState();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unbindService(musicServiceConnection);
		Log.d(DEBUG, "Service unbound ...");
		musicServiceBound = false;
	}

	@Override
	protected void onStop() {
		Log.d(DEBUG, "Activity stopped !");
		super.onStop();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_player);

		// Enable title bar back button
		if (getSupportActionBar() != null) {
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}

		dbHelper = DatabaseHelper.getInstance(this);
		playbackSettings = dbHelper.getContentValues(DatabaseHelper.PLAYBACK_SETTINGS_TABLE);

		// [!WeakReference] Initialising
		weakActivity = new WeakReference<>(PlayerActivity.this);

		audioFileProgressBar = findViewById(R.id.audioFileProgressBar);

		btnPlayPause = findViewById(R.id.btnPlayPause);
		btnPrevious = findViewById(R.id.btnPrevious);
		btnNext = findViewById(R.id.btnNext);
		btnShuffle = findViewById(R.id.btnShuffle);
		btnRepeat = findViewById(R.id.btnRepeat);

		txtCurrentArtist = findViewById(R.id.txtCurrentArtist);
		txtCurrentTitle = findViewById(R.id.txtCurrentTitle);
		txtDurationCurrent = findViewById(R.id.txtDurationCurrent);
		txtDurationTotal = findViewById(R.id.txtDurationTotal);

		ivAlbumArt = findViewById(R.id.ivAlbumArt);
		bookMarkSection = findViewById(R.id.bookmark_section);
        albumArtContainer = findViewById(R.id.album_art_container);
        fabAddBM = findViewById(R.id.bookmark_add_fab);
        fabTurnToBack = findViewById(R.id.bookmark_turn_to_back_fab);
        fabTurnToFront = findViewById(R.id.bookmark_turn_to_front_fab);

		float scale = getApplicationContext().getResources().getDisplayMetrics().density;

		// Passing W and H of album art to bookmark section
		ViewTreeObserver viewTreeObserver = ivAlbumArt.getViewTreeObserver();
		viewTreeObserver.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
			@Override
			public boolean onPreDraw() {
				ivAlbumArt.getViewTreeObserver().removeOnPreDrawListener(this);
				int finalHeight = ivAlbumArt.getMeasuredHeight();
				int finalWidth = ivAlbumArt.getMeasuredWidth();
				int maxXY = Math.min(finalHeight, finalWidth);
				bookMarkSection.setMinimumWidth(maxXY);
				bookMarkSection.setMinimumHeight(maxXY);
				bookMarkSection.setMaxWidth(maxXY);
				bookMarkSection.setMaxHeight(maxXY);
				albumArtContainer.setMinimumWidth(maxXY);
				albumArtContainer.setMinimumHeight(maxXY);
				albumArtContainer.setMaxHeight(maxXY);
				albumArtContainer.setMaxWidth(maxXY);

				return true;
			}
		});

        albumArtContainer.setCameraDistance(scale * 8000);
		ivAlbumArt.setCameraDistance(scale * 8000);
		bookMarkSection.setCameraDistance(scale * 8000);

		frontAnim = (AnimatorSet) AnimatorInflater.loadAnimator(getApplicationContext(), R.animator.front_animator);
		backAnim = (AnimatorSet) AnimatorInflater.loadAnimator(getApplicationContext(), R.animator.back_animator);

		btnShuffle.setTag(playbackSettings.get(PlayMode.MODE_SHUFFLE));
		btnRepeat.setTag(playbackSettings.get(PlayMode.MODE_REPEAT));

		handler.postDelayed(updateProgressBar, 100);

		btnShuffle.setOnClickListener(this::onBtnShuffleClick);
		btnRepeat.setOnClickListener(this::onBtnRepeatClick);
		btnPlayPause.setOnClickListener(view -> togglePlayPause());
		btnPrevious.setOnClickListener(view -> playPreviousPosition());
		btnNext.setOnClickListener(view -> playNextPosition());
		audioFileProgressBar.setOnSeekBarChangeListener(onSeekBarChangeListener);

		bookMarkRecView = findViewById(R.id.bookmark_rec_view);
		bookmarkAdapter.setBookmarkList(bookmarkList);
		bookMarkRecView.setLayoutManager(new LinearLayoutManager(this));
		bookMarkRecView.setAdapter(bookmarkAdapter);

		fabTurnToBack.setOnClickListener(fabTurnToBackClicked -> {
            if (isFront) {
	            albumArtContainer.setClickable(false);
	            fabTurnToBack.setClickable(false);
	            frontAnim.setTarget(albumArtContainer);
                backAnim.setTarget(bookMarkSection);
                frontAnim.start();
                backAnim.start();
                isFront = false;
            }
		});

		fabTurnToFront.setOnClickListener(fabTurnToFrontClicked -> {
			if (!isFront) {
				albumArtContainer.setClickable(true);
				fabTurnToBack.setClickable(true);
				frontAnim.setTarget(bookMarkSection);
				backAnim.setTarget(albumArtContainer);
				frontAnim.start();
				backAnim.start();
				isFront = true;
			}
		});

		fabAddBM.setOnClickListener(fabClicked -> {

			// To prevent delays caused by user input
			int snapshotCurrentPos = musicService.getCurrentPosition();
			int snapshotCurrentAudioFileId = musicService.getCurrentAudioFile().getId();

			AlertDialog.Builder builder = MyBuilder.createGenericDialogBuilder(this);
			builder.setCancelable(false);
			builder.setTitle("Enter a note (" + Formatter.msToMinutesAndSeconds(snapshotCurrentPos, false) + ")");

			final EditText inputView = new EditText(PlayerActivity.this);
			inputView.setInputType(InputType.TYPE_CLASS_TEXT);
			builder.setView(inputView);

			//Setting OnClickListeners to negative and positive Response Buttons
			builder.setPositiveButton("Create Bookmark", (dialog, which) -> {
				String inputText = inputView.getText().toString().trim();
				if (!inputText.equals("")) {
					dbHelper.addBookmark(new Bookmark(snapshotCurrentAudioFileId, snapshotCurrentPos, inputText));
					// Update list to the very current audio (not the snapshot), in case dialog submit is missing until after the song has changed
					bookmarkAdapter.setBookmarkList(dbHelper.getBookmarkList(musicService.getCurrentAudioFile().getId()));
				} else {
					MyBuilder.createToast(this, MyBuilder.TOAST_NOTIFICATION, "Bookmark not saved.").show();
				}
			});
			builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
			builder.show();

		});

		updateBtnPlayPauseState();
		updateBtnPlayModeState();
	}



	/***************************************************
	 Player Controls
	 ***************************************************/

	public void playNextPosition() {
		Intent musicServiceIntent = new Intent(this, MusicService.class);
		musicServiceIntent.setAction(MusicService.ACTION_NEXT);
		startService(musicServiceIntent);
	}

	public void playPreviousPosition() {
		Intent musicServiceIntent = new Intent(this, MusicService.class);
		musicServiceIntent.setAction(MusicService.ACTION_PREV);
		startService(musicServiceIntent);
	}

	public void togglePlayPause() {
		Intent musicServiceIntent = new Intent(this, MusicService.class);
		musicServiceIntent.setAction(MusicService.ACTION_PAUSE);
		startService(musicServiceIntent);
		updateBtnPlayPauseState();
	}

	public void onBtnShuffleClick(View view) {
		String newMode;

		if (btnShuffle.getTag().equals(PlayMode.MODE_INACTIVE)) {
			newMode = PlayMode.MODE_ACTIVE;
		} else {
			newMode = PlayMode.MODE_INACTIVE;
		}

		btnShuffle.setTag(newMode);
		musicService.setShuffleMode(newMode);
		updateBtnPlayModeState();
	}

	public void onBtnRepeatClick(View view) {
		String newMode;

		switch (btnRepeat.getTag().toString()) {
			case PlayMode.REPEAT_ALL:
				newMode = PlayMode.REPEAT_ONE;
				break;
			case PlayMode.REPEAT_ONE:
				newMode = PlayMode.MODE_INACTIVE;
				break;
			default:
				newMode = PlayMode.REPEAT_ALL;
				break;
		}

		btnRepeat.setTag(newMode);
		musicService.setRepeatMode(newMode);
		updateBtnPlayModeState();
	}



	/***************************************************
	 Update UI components
	 ***************************************************/

	private void pullPlaybackSettings() {
		this.playbackSettings = dbHelper.getContentValues(DatabaseHelper.PLAYBACK_SETTINGS_TABLE);
	}

	public void updateCurrentAudioFile(AudioFile newAudioFile) {
		try {
			Bitmap coverArt = Helper.extractAlbumArt(newAudioFile.getDataPath());
			ImageView playerBackgroundLayer = findViewById(R.id.player_background_layer);

			if (coverArt != null) {
				// Setting blurred album art as background
				// Causes E/ANDR-PERF: IPerf::tryGetService failed! - But so does Blurry from GitHub.
				Bitmap blurredAlbum = MyBuilder.blur(this, coverArt);
				playerBackgroundLayer.setImageBitmap(blurredAlbum);
				ivAlbumArt.setImageBitmap(coverArt);
			} else {
				playerBackgroundLayer.setImageBitmap(null);
				ivAlbumArt.setImageResource(R.drawable.album_art_blank);
			}

			if (Helper.deviceIsInLandscape(this)) {
				MediaStoreHelper mediaStoreHelper = new MediaStoreHelper(this);
				AudioFileExtras audioFileExtras = mediaStoreHelper.getAudioFileExtras(newAudioFile.getId());
				String bitRate = Helper.getBitRateInKBPS(newAudioFile.getDataPath());

				TextView txtAudioAlbum = findViewById(R.id.txtAudioAlbum);
				TextView txtAudioYear = findViewById(R.id.txtAudioYear);
				TextView txtAudioGenre = findViewById(R.id.txtAudioGenre);
				TextView txtAudioBitRate = findViewById(R.id.txtAudioBitRate);
				TextView txtAudioDateAdded = findViewById(R.id.txtAudioDateAdded);
				TextView txtAudioSize = findViewById(R.id.txtAudioSize);
				TextView txtAudioFileName = findViewById(R.id.txtAudioFileName);

				txtAudioAlbum.setText(audioFileExtras.getAlbum());
				txtAudioYear.setText(audioFileExtras.getYear());
				txtAudioGenre.setText(audioFileExtras.getGenre());
				txtAudioBitRate.setText(bitRate);
				txtAudioDateAdded.setText(Formatter.unixTimeToReadable(audioFileExtras.getDateAdded()));
				txtAudioSize.setText(Formatter.bytesToMegabytes(audioFileExtras.getSize()));
				txtAudioFileName.setText(audioFileExtras.getFileName());;
			}

			txtCurrentArtist.setText(newAudioFile.getArtist());
			txtCurrentTitle.setText(newAudioFile.getTitle());
			String durationFormatted = Formatter.msToMinutesAndSeconds(newAudioFile.getDuration(), true);
			txtDurationTotal.setText(durationFormatted);
			audioFileProgressBar.setMax(newAudioFile.getDuration());

			dbHelper.getBookmarkList(newAudioFile.getId());

			bookMarkRecView = findViewById(R.id.bookmark_rec_view);
			bookmarkList = dbHelper.getBookmarkList(newAudioFile.getId());
			bookmarkAdapter.setBookmarkList(bookmarkList);
			bookmarkAdapter.setSongLengthInMs(newAudioFile.getDuration());
			bookMarkRecView.setLayoutManager(new LinearLayoutManager(this));
			bookMarkRecView.setAdapter(bookmarkAdapter);

			Log.d(DEBUG, "Player UI fed with new track ...");
			updateBtnPlayPauseState();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateBtnPlayPauseState() {
		if (musicServiceBound) {
			if (!musicService.isPlaying()) {
				btnPlayPause.setImageResource(R.drawable.button_play);
			} else {
				btnPlayPause.setImageResource(R.drawable.button_pause);
			}
		}
	}

	public void updateBtnPlayModeState() {
		pullPlaybackSettings();
		if (playbackSettings.get(PlayMode.MODE_SHUFFLE).equals(PlayMode.MODE_ACTIVE)) {
			btnShuffle.setTag(PlayMode.MODE_ACTIVE);
			btnShuffle.setImageResource(R.drawable.button_shuffle_active);
		} else {
			btnShuffle.setTag(PlayMode.MODE_INACTIVE);
			btnShuffle.setImageResource(R.drawable.button_shuffle_inactive);
		}

		switch (playbackSettings.get(PlayMode.MODE_REPEAT).toString()) {
			case PlayMode.REPEAT_ALL:
				btnRepeat.setTag(PlayMode.REPEAT_ALL);
				btnRepeat.setImageResource(R.drawable.button_repeat_all);
				break;
			case PlayMode.REPEAT_ONE:
				btnRepeat.setTag(PlayMode.REPEAT_ONE);
				btnRepeat.setImageResource(R.drawable.button_repeat_one);
				break;
			default:
				btnRepeat.setTag(PlayMode.MODE_INACTIVE);
				btnRepeat.setImageResource(R.drawable.button_repeat_inactive);
		}
	}
}