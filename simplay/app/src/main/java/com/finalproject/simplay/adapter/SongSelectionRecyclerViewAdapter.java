package com.finalproject.simplay.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.simplay.R;
import com.finalproject.simplay.model.AudioFile;
import com.turingtechnologies.materialscrollbar.INameableAdapter;

import java.util.ArrayList;

public class SongSelectionRecyclerViewAdapter extends RecyclerView.Adapter<SongSelectionRecyclerViewAdapter.SongSelectionViewHolder>
    implements INameableAdapter {

    private ArrayList<AudioFile> musicList;
    private final ArrayList<AudioFile> selectedList = new ArrayList<>();

    public void setMusicList(ArrayList<AudioFile> musicList) {
        this.musicList = musicList;
    }


    public ArrayList<AudioFile> getSelectedList() {
        return selectedList;
    }

    @NonNull
    @Override
    public SongSelectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_song_selection, parent, false);
        return new SongSelectionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SongSelectionViewHolder holder, int position) {
        CheckBox cBox = holder.cBoxSongTitle;
        AudioFile currentItem = musicList.get(position);

        holder.txtSongArtist.setText(currentItem.getArtist());
        cBox.setText(currentItem.getTitle());
        holder.txtSongArtist.setSingleLine(true);
        cBox.setSingleLine(true);
        cBox.setChecked(currentItem.isSelected());

        //cBox.setOnCheckedChangeListener(null);
        cBox.setOnClickListener(view -> {

            if (cBox.isChecked()) {
                selectedList.add(currentItem);
            } else {
                selectedList.remove(currentItem);
            }
            currentItem.setSelected(cBox.isChecked());
        });
    }

    @Override
    public int getItemCount() {
        return musicList.size();
    }

    @Override
    public Character getCharacterForElement(int element) {
        char c = musicList.get(element).getArtist().charAt(0);
        if (Character.isDigit(c)) {
            c = '#';
        }
        return c;
    }

    public static class SongSelectionViewHolder extends RecyclerView.ViewHolder {
        private final CheckBox cBoxSongTitle;
        private final TextView txtSongArtist;

        public SongSelectionViewHolder(@NonNull View itemView) {
            super(itemView);
            cBoxSongTitle = itemView.findViewById(R.id.song_selection_item_checkbox);
            txtSongArtist = itemView.findViewById(R.id.song_selection_artist);
        }
    }
}
