package com.finalproject.simplay.service;

import android.media.AudioAttributes;
import android.media.MediaPlayer;

public class MyMediaPlayer extends MediaPlayer {

	private static MyMediaPlayer instance;

	// Extending MediaPlayer to apply singleton pattern
	public static synchronized MyMediaPlayer getInstance() {
		if (instance == null) {
			instance = new MyMediaPlayer();
			instance.setAudioAttributes(
					new AudioAttributes.Builder()
							.setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
							.setUsage(AudioAttributes.USAGE_MEDIA)
							.build()
			);

		}
		return instance;
	}

	private MyMediaPlayer() {
		super();
	}

}
