package com.finalproject.simplay.callback;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.simplay.adapter.PlaylistSongsRecyclerViewAdapter;

public class PlaylistMoveCallback extends ItemTouchHelper.Callback {

    private PlaylistSongsRecyclerViewTouchHelper touchHelper;

    public PlaylistMoveCallback(PlaylistSongsRecyclerViewTouchHelper touchHelper) {
        this.touchHelper = touchHelper;
    }


    @Override
    public boolean isLongPressDragEnabled() {
        return true;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return false;
    }

    @Override
    public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        int dragFlag = ItemTouchHelper.UP | ItemTouchHelper.DOWN ;
        return makeMovementFlags(dragFlag, 0);
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        this.touchHelper.onRowMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSelectedChanged(@Nullable RecyclerView.ViewHolder viewHolder, int actionState) {
        if (actionState != ItemTouchHelper.ACTION_STATE_IDLE && viewHolder instanceof PlaylistSongsRecyclerViewAdapter.PlaylistSongViewHolder) {
            PlaylistSongsRecyclerViewAdapter.PlaylistSongViewHolder myViewHolder = (PlaylistSongsRecyclerViewAdapter.PlaylistSongViewHolder) viewHolder;
            touchHelper.onRowSelected(myViewHolder);
        }
        super.onSelectedChanged(viewHolder, actionState);
    }

    @Override
    public void clearView(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        if (viewHolder instanceof PlaylistSongsRecyclerViewAdapter.PlaylistSongViewHolder) {
            PlaylistSongsRecyclerViewAdapter.PlaylistSongViewHolder myviewHolder = (PlaylistSongsRecyclerViewAdapter.PlaylistSongViewHolder) viewHolder;
            touchHelper.onRowClear(myviewHolder);
        }
        super.clearView(recyclerView, viewHolder);
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

    }

    public interface PlaylistSongsRecyclerViewTouchHelper {
        void onRowMoved(int from, int to);

        void onRowSelected(PlaylistSongsRecyclerViewAdapter.PlaylistSongViewHolder holder);

        void onRowClear(PlaylistSongsRecyclerViewAdapter.PlaylistSongViewHolder holder);
    }
}
