package com.finalproject.simplay.tabfragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.finalproject.simplay.R;
import com.finalproject.simplay.adapter.SongSelectionRecyclerViewAdapter;
import com.finalproject.simplay.model.AudioFile;
import com.finalproject.simplay.repository.DatabaseHelper;
import com.finalproject.simplay.repository.MediaStoreHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.turingtechnologies.materialscrollbar.AlphabetIndicator;
import com.turingtechnologies.materialscrollbar.DragScrollBar;

import java.util.ArrayList;

//TODO Back Button


public class SongSelectionFragment extends Fragment {

	private ArrayList<AudioFile> audioFiles;
	RecyclerView recyclerView;
	SongSelectionRecyclerViewAdapter adapter = new SongSelectionRecyclerViewAdapter();
	String intention;


	@Override
	public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		if (intention.equals("add")) {
			menu.findItem(R.id.action_search).setVisible(true);
			SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
			searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
				@Override
				public boolean onQueryTextSubmit(String query) {
					return false;
				}

				@Override
				public boolean onQueryTextChange(String newText) {
					if (newText.equals("")) {
						adapter.setMusicList(audioFiles);
					} else {
						ArrayList<AudioFile> tempMusicList = new ArrayList<>();
						for (AudioFile audioFile : audioFiles) {
							if (audioFile.getArtist().toUpperCase().contains(newText.toUpperCase()) || audioFile.getTitle().toUpperCase().contains(newText.toUpperCase())) {
								tempMusicList.add(audioFile);
							}
						}
						adapter.setMusicList(tempMusicList);
					}
					recyclerView.setAdapter(adapter);
					return false;
				}
			});
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_song_selection, container, false);
		recyclerView = view.findViewById(R.id.song_selection_recycler_view);
		FloatingActionButton fab = view.findViewById(R.id.song_selection_fab);
		audioFiles = new ArrayList<>();

		setHasOptionsMenu(true);

		//Receiving String of the Table, where new Songs need to be inserted
		Bundle args = this.getArguments();
		assert args != null;
		String tableName = args.getString("playlistName");
		intention = args.getString("intention");

		DragScrollBar scrollBar = view.findViewById(R.id.scrollBar);

		//initialising URI depending on build version
		if (intention.equals("add")) {
			fab.setImageResource(R.drawable.ic_edit_playlist_add);

			MediaStoreHelper mediaStoreHelper = new MediaStoreHelper(requireActivity().getApplicationContext());
			audioFiles = mediaStoreHelper.getAudioFiles();

			//Setting up RecViewAdapter
			adapter.setMusicList(audioFiles);
			recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
			recyclerView.setAdapter(adapter);

			scrollBar.setIndicator(new AlphabetIndicator(view.getContext()), false);

			fab.setOnClickListener(fabClicked -> {
				ArrayList<AudioFile> newSongs;
				newSongs = adapter.getSelectedList();

				//Adding new Songs to Playlist
				DatabaseHelper databaseHelper = DatabaseHelper.getInstance(getContext());
				databaseHelper.addToPlaylist(tableName, newSongs);
				databaseHelper.close();

				//Navigating back to PlaylistSongFragment
				Navigation.findNavController(fabClicked).popBackStack();
			});
		} else {
			fab.setImageResource(R.drawable.ic_edit_playlist_remove);
			DatabaseHelper databaseHelper = DatabaseHelper.getInstance(getContext());
			audioFiles = databaseHelper.getPlaylist(tableName);

			SongSelectionRecyclerViewAdapter adapter = new SongSelectionRecyclerViewAdapter();
			adapter.setMusicList(audioFiles);
			recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
			recyclerView.setAdapter(adapter);

			fab.setOnClickListener(fabClicked -> {
				ArrayList<AudioFile> songList;
				songList = adapter.getSelectedList();

				databaseHelper.deleteFromPlaylist(tableName, songList);
				Navigation.findNavController(fabClicked).popBackStack();
			});
		}

		scrollBar.setRecyclerView(recyclerView);

		return view;
	}
}