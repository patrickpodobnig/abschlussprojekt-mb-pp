package com.finalproject.simplay.repository;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;

import com.finalproject.simplay.model.AudioFile;
import com.finalproject.simplay.model.AudioFileExtras;
import com.finalproject.simplay.model.Genre;
import com.finalproject.simplay.util.Formatter;

import java.util.ArrayList;

public class MediaStoreHelper {

	private final Context calledByContext;

	private static final String[] audioFileProjections = {
			MediaStore.Audio.Media._ID,
			MediaStore.Audio.Media.ARTIST,
			MediaStore.Audio.Media.TITLE,
			MediaStore.Audio.Media.DURATION,
			MediaStore.Audio.Media.DATA
	};

	private static final String[] audioFileExtrasProjections = {
			MediaStore.Audio.Media.ALBUM,
			MediaStore.Audio.Media.YEAR,
			MediaStore.Audio.Media.DATE_ADDED,
			MediaStore.Audio.Media.SIZE,
			MediaStore.Audio.Media.DISPLAY_NAME
	};

	public static final String COLLECTION_MEDIA = "mediastorehelper.collection.media";
	public static final String COLLECTION_GENRES = "mediastorehelper.collection.genres";
	public static final String FILTER_ARTIST = "mediastorehelper.filter.artist";
	public static final String FILTER_GENRE = "mediastorehelper.filter.genre";

	public final static String SELECTION_IS_MUSIC = MediaStore.Audio.AudioColumns.IS_MUSIC + " != 0";

//	public static MediaStoreHelper getInstance(Context calledByContext) {
//		if (instance == null) {
//			instance = new MediaStoreHelper(calledByContext);
//		}
//		return instance;
//	}
//
//	private MediaStoreHelper(Context calledByContext) {
//		this.calledByContext = calledByContext.getApplicationContext();
//	}


	public MediaStoreHelper(Context calledByContext) {
		this.calledByContext = calledByContext;
	}

	public ArrayList<AudioFile> getAudioFiles() {
		ArrayList<AudioFile> audioFiles = new ArrayList<>();
		Uri collection = getCollectionUri(COLLECTION_MEDIA);

		try (Cursor audioCursor = calledByContext.getApplicationContext().getContentResolver().query(
				collection,
				audioFileProjections,
				SELECTION_IS_MUSIC,
				null,
				MediaStore.Audio.Media.ARTIST + ", " + MediaStore.Audio.Media.TITLE
		)) {
			int idCol = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media._ID);
			int artistCol = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST);
			int titleCol = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE);
			int durationCol = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION);
			int dataPathCol = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);

			while (audioCursor.moveToNext()) {
				int id = audioCursor.getInt(idCol);
				String artist = audioCursor.getString(artistCol);
				String title = audioCursor.getString(titleCol);
				int duration = audioCursor.getInt(durationCol);
				String dataPath = audioCursor.getString(dataPathCol);

				audioFiles.add(new AudioFile(id, artist, title, duration, dataPath));
			}

			return audioFiles;
		}
	}



	public ArrayList<AudioFile> getAudioFiles(String filter, Object argument) {
		ArrayList<AudioFile> audioFiles = new ArrayList<>();
		String query = "";
		Uri collection;
		switch (filter) {
			case FILTER_ARTIST:
				collection = getCollectionUri(COLLECTION_MEDIA);
				query =  " AND " + MediaStore.Audio.Media.ARTIST + " = " + Formatter.prepareDblQuotedString((String) argument);
				break;
			case FILTER_GENRE:
				collection = MediaStore.Audio.Genres.Members.getContentUri("external", (long) argument);
				break;
			default:
				throw new RuntimeException("Invalid filter !");
		}

		try (Cursor audioCursor = calledByContext.getApplicationContext().getContentResolver().query(
				collection,
				audioFileProjections,
				SELECTION_IS_MUSIC + query,
				null,
				MediaStore.Audio.Media.ARTIST + ", " + MediaStore.Audio.Media.TITLE
		)) {
			int idCol = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media._ID);
			int artistCol = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST);
			int titleCol = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE);
			int durationCol = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION);
			int dataPathCol = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);

			while (audioCursor.moveToNext()) {
				int id = audioCursor.getInt(idCol);
				String artist = audioCursor.getString(artistCol);
				String title = audioCursor.getString(titleCol);
				int duration = audioCursor.getInt(durationCol);
				String dataPath = audioCursor.getString(dataPathCol);

				audioFiles.add(new AudioFile(id, artist, title, duration, dataPath));
			}

			return audioFiles;
		}
	}



	public AudioFileExtras getAudioFileExtras(int audioFileId) {
		Uri mediaCollection = getCollectionUri(COLLECTION_MEDIA);
		String selection = MediaStore.Audio.Media._ID + " = " + audioFileId;

		try (Cursor audioCursor = calledByContext.getApplicationContext().getContentResolver().query(
				mediaCollection,
				audioFileExtrasProjections,
				selection,
				null,
				null
		)) {
			int albumCol = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM);
			int yearCol = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.YEAR);
			int dateAddedCol = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATE_ADDED);
			int fileNameCol = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DISPLAY_NAME);
			int sizeCol = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.SIZE);

			audioCursor.moveToFirst();

			String album = audioCursor.getString(albumCol);
			String year = audioCursor.getString(yearCol);
			int dateAdded = audioCursor.getInt(dateAddedCol);
			String fileName = audioCursor.getString(fileNameCol);
			long size = audioCursor.getInt(sizeCol);

			Uri genreCollection = MediaStore.Audio.Genres.getContentUriForAudioId("external", audioFileId);
			String[] genreProjection = {MediaStore.Audio.Genres.NAME};
			try (Cursor genreCursor = calledByContext.getApplicationContext().getContentResolver().query(
					genreCollection,
					genreProjection,
					null,
					null,
					null
			)) {
				int genreCol = genreCursor.getColumnIndexOrThrow(MediaStore.Audio.Genres.NAME);
				String genre = "";
				if (genreCursor.moveToFirst()) {
					genre = genreCursor.getString(genreCol);
				}

				return new AudioFileExtras(album, year, genre, dateAdded, fileName, size);
			}
		}
	}



	public ArrayList<String> getArtists() {
		ArrayList<String> artists = new ArrayList<>();
		Uri collection = getCollectionUri(COLLECTION_MEDIA);

		String[] projections = {
				MediaStore.Audio.Media.ARTIST
		};

		//Setting up query to get all songs
		try (Cursor audioCursor = calledByContext.getContentResolver().query(
				collection,
				projections,
				SELECTION_IS_MUSIC,
				null,
				MediaStore.Audio.Artists.ARTIST
		)) {
			int artistCol = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST);

			while (audioCursor.moveToNext()) {
				String artist = audioCursor.getString(artistCol);
				if (!artists.contains(artist))
					artists.add(artist);
			}

			return artists;
		}

	}


	public ArrayList<Genre> getGenres() {
		ArrayList<Genre> genres = new ArrayList<>();
		Uri collection = getCollectionUri(COLLECTION_GENRES);
		String[] projections = {
				MediaStore.Audio.Genres._ID,
				MediaStore.Audio.Genres.NAME
		};

		try (Cursor audioCursor = calledByContext.getContentResolver().query(
				collection,
				projections,
				MediaStore.Audio.Genres._ID + " != 0",
				null,
				MediaStore.Audio.Genres.NAME
		)) {
			int genreIdCol = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Genres._ID);
			int genreCol = audioCursor.getColumnIndexOrThrow(MediaStore.Audio.Genres.NAME);

			while (audioCursor.moveToNext()) {
				long genreId = audioCursor.getLong(genreIdCol);
				String genre = audioCursor.getString(genreCol);
				if (genre != null) {
					genres.add(new Genre(genreId, genre));
				} else {
					genres.add(new Genre(999, "Undefined"));
				}
			}

			return genres;
		}

	}



	private Uri getCollectionUri(String table) {
		Uri collection;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
			switch (table) {
				case COLLECTION_GENRES:
					collection = MediaStore.Audio.Genres.getContentUri(MediaStore.VOLUME_EXTERNAL);
					break;
				case COLLECTION_MEDIA:
					collection = MediaStore.Audio.Media.getContentUri(MediaStore.VOLUME_EXTERNAL);
					break;
				default:
					return null;
			}
		} else {
			switch (table) {
				case COLLECTION_GENRES:
					collection = MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI;
					break;
				case COLLECTION_MEDIA:
					collection = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
					break;
				default:
					return null;
			}
		}
		return collection;
	}

}
