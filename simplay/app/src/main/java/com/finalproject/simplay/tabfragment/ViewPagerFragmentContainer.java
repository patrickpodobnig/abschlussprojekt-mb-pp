package com.finalproject.simplay.tabfragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.finalproject.simplay.R;
import com.finalproject.simplay.adapter.TabFragmentAdapter;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;


public class ViewPagerFragmentContainer extends Fragment {


    public ViewPagerFragmentContainer() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_view_pager_container, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //setting up TabLayout with ViewPager2
        TabLayout tabLayout = view.findViewById(R.id.tab_layout);
        ViewPager2 viewPager2 = view.findViewById(R.id.view_pager);


        viewPager2.setAdapter(new TabFragmentAdapter(getChildFragmentManager(), getViewLifecycleOwner().getLifecycle()));

        //initialising Tabs
        new TabLayoutMediator(tabLayout, viewPager2,
                (tab, position) -> {
                    switch (position) {
                        case 1:
                            tab.setText(R.string.genre_tab);
                            break;
                        case 2:
                            tab.setText(R.string.artist_tab);
                            break;
                        case 3:
                            tab.setText(R.string.all_songs_tab);
                            break;
                        default:
                            tab.setText(R.string.playlist_tab);
                            break;
                    }
                }
        ).attach();
    }
}