package com.finalproject.simplay;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.simplay.adapter.CurrentPlaylistRecViewAdapter;
import com.finalproject.simplay.model.AudioFile;
import com.finalproject.simplay.service.MusicService;
import com.turingtechnologies.materialscrollbar.AlphabetIndicator;
import com.turingtechnologies.materialscrollbar.DragScrollBar;

import java.util.ArrayList;

public class CurrentPlaylistActivity extends AppCompatActivity {

	private ArrayList<AudioFile> musicItemList;
	CurrentPlaylistRecViewAdapter adapter;
	private RecyclerView recyclerView;

	private MusicService musicService;
	private boolean musicServiceBound = false;
	private DragScrollBar scrollBar;

	private final ServiceConnection musicServiceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
			MusicService.MusicBinder musicBinder = (MusicService.MusicBinder) iBinder;
			musicService = musicBinder.getService();

			musicItemList = musicService.getCurrentPlaylist();
			adapter = new CurrentPlaylistRecViewAdapter(musicItemList);
			recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

			recyclerView.setAdapter(adapter);
			musicServiceBound = true;

			scrollBar.setIndicator(new AlphabetIndicator(getApplicationContext()), false);
			updateScrollView();
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			musicServiceBound = false;
		}
	};



	/***************************************************
	 Activity Overrides
	 ***************************************************/

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	protected void onStart() {
		super.onStart();
		Intent musicServiceIntent = new Intent(this, MusicService.class);
		bindService(musicServiceIntent, musicServiceConnection, BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unbindService(musicServiceConnection);
		musicServiceBound = false;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_current_playlist);

		// Enable title bar back button
		if (getSupportActionBar() !=  null) {
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}

		recyclerView = findViewById(R.id.current_playlist_recycler_view);

		scrollBar = findViewById(R.id.scrollBar);
		scrollBar.setRecyclerView(recyclerView);

	}

	private void updateScrollView() {
		if (musicServiceBound) {
			int goToPos = musicService.getCurrentPlaylistPos();
			recyclerView.scrollToPosition(goToPos);
			adapter.setSelectedPosition(goToPos);
		}
	}
}
