package com.finalproject.simplay.adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.core.content.ContextCompat;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.simplay.R;
import com.finalproject.simplay.callback.PlaylistMoveCallback;
import com.finalproject.simplay.model.AudioFile;
import com.finalproject.simplay.service.MusicService;
import com.finalproject.simplay.repository.DatabaseHelper;
import com.finalproject.simplay.util.MyBuilder;
import com.finalproject.simplay.util.Formatter;
import com.finalproject.simplay.util.Helper;
import com.turingtechnologies.materialscrollbar.INameableAdapter;

import java.util.ArrayList;
import java.util.Collections;

public class PlaylistSongsRecyclerViewAdapter extends RecyclerView.Adapter<PlaylistSongsRecyclerViewAdapter.PlaylistSongViewHolder>
		implements PlaylistMoveCallback.PlaylistSongsRecyclerViewTouchHelper, INameableAdapter {
	ArrayList<AudioFile> playlistSongs = new ArrayList<>();
	String playlistTableName;

	private MusicService musicService;

	private final ServiceConnection musicServiceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
			MusicService.MusicBinder musicBinder = (MusicService.MusicBinder) iBinder;
			musicService = musicBinder.getService();
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
		}
	};

	public PlaylistSongsRecyclerViewAdapter(String playlistTableName) {
		this.playlistTableName = playlistTableName;
	}

	@SuppressLint("NotifyDataSetChanged")
	public void setPlaylistSongs(ArrayList<AudioFile> playlistSongs) {
		this.playlistSongs = playlistSongs;
		notifyDataSetChanged();
	}

	@NonNull
	@Override
	public PlaylistSongViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		Intent musicServiceIntent = new Intent(parent.getContext(), MusicService.class);
		parent.getContext().bindService(musicServiceIntent, musicServiceConnection, Context.BIND_AUTO_CREATE);
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_audio_file, parent, false);
		return new PlaylistSongViewHolder(view);
	}

	@Override
	public void onBindViewHolder(@NonNull PlaylistSongViewHolder holder, @SuppressLint("RecyclerView") int position) {
		AudioFile currentItem = playlistSongs.get(position);
		String durationFormatted = Formatter.msToMinutesAndSeconds(currentItem.getDuration(), false);

		holder.songArtist.setText(currentItem.getArtist());
		holder.songTitle.setText(currentItem.getTitle());
		holder.songArtist.setSingleLine(true);
		holder.songTitle.setSingleLine(true);
		holder.songDuration.setText(durationFormatted);

		holder.itemView.setOnClickListener(view -> {
			musicService.setCurrentPlaylist(playlistSongs);

			Intent musicServiceIntent = new Intent(view.getContext(), MusicService.class);
			musicServiceIntent.putExtra("audioFileListPos", position);
			musicServiceIntent.setAction(MusicService.ACTION_PLAY_POSITION);
			view.getContext().startService(musicServiceIntent);

			Navigation.findNavController(view).navigate(R.id.action_playlistSongs_to_playerActivity2);
		});

		holder.kebapMenuButton.setOnClickListener(kebapMenuButtonClicked -> {
			PopupMenu popupMenu = MyBuilder.createGenericPopup(holder.itemView.getContext(), holder.kebapMenuButton);
			popupMenu.getMenuInflater().inflate(R.menu.menu_dropdown, popupMenu.getMenu());
			popupMenu.getMenu().add("Remove from Playlist");
			popupMenu.getMenu().add("Add to Playlist");
			popupMenu.getMenu().add("Play Next");
			popupMenu.setOnMenuItemClickListener(menuItemClicked -> {

				DatabaseHelper dbHelper = DatabaseHelper.getInstance(holder.itemView.getContext());

				if (menuItemClicked.getTitle().equals("Remove from Playlist")) {
					dbHelper.deleteFromPlaylist(playlistTableName, currentItem);
					// Refresh
					setPlaylistSongs(dbHelper.getPlaylist(playlistTableName));

				} else if (menuItemClicked.getTitle().equals("Add to Playlist")) {
					AlertDialog dialog = MyBuilder.createAddToPlaylistDialog(holder.itemView, currentItem, "PlaylistSongs");
					dialog.show();
					// Refresh on dismissal in case of target playlist == host playlist
					dialog.setOnDismissListener(dialogClicked -> setPlaylistSongs(dbHelper.getPlaylist(playlistTableName)));
				} else if (menuItemClicked.getTitle().equals("Play Next")) {
					musicService.playNext(currentItem);
				}
				return false;
			});

			popupMenu.show();
		});

	}

	@Override
	public int getItemCount() {
		return playlistSongs.size();
	}

	@Override
	public void onRowMoved(int from, int to) {
		if (from < to) {
			for (int i = from; i < to; i++) {
				Collections.swap(playlistSongs, i, i + 1);
			}
		} else {
			for (int i = from; i > to; i--) {
				Collections.swap(playlistSongs, i, i - 1);
			}
		}
		notifyItemMoved(from, to);
	}

	@Override
	public void onRowSelected(PlaylistSongViewHolder holder) {
		holder.itemView.setScaleX(1.1f);
		//holder.itemView.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), (R.color.white_transparent)));
	}

	@SuppressLint("NotifyDataSetChanged")
	@Override
	public void onRowClear(PlaylistSongViewHolder holder) {
		holder.itemView.setScaleX(1);
		//holder.itemView.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), (R.color.black_transparent)));

		DatabaseHelper databaseHelper = DatabaseHelper.getInstance(holder.itemView.getContext());
		databaseHelper.updatePlaylist(playlistTableName, playlistSongs);
		databaseHelper.close();
		notifyDataSetChanged();
	}

	@Override
	public Character getCharacterForElement(int element) {
		char c = playlistSongs.get(element).getArtist().charAt(0);

		if (Character.isDigit(c)) {
			c = '#';
		}
		return c;
	}

	public static class PlaylistSongViewHolder extends RecyclerView.ViewHolder {
		private final TextView songTitle;
		private final TextView songArtist;
		private final TextView songDuration;
		private final AppCompatImageButton kebapMenuButton;


		public PlaylistSongViewHolder(@NonNull View itemView) {
			super(itemView);
			songTitle = itemView.findViewById(R.id.txtAudioTitle);
			songArtist = itemView.findViewById(R.id.txtAudioArtist);
			songDuration = itemView.findViewById(R.id.txtAudioDuration);
			kebapMenuButton = itemView.findViewById(R.id.dropDownToggleSongItems);
		}
	}
}
