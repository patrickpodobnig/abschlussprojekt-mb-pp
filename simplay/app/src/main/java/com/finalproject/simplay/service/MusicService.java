package com.finalproject.simplay.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.finalproject.simplay.CategoryActivity;
import com.finalproject.simplay.PlayerActivity;
import com.finalproject.simplay.R;
import com.finalproject.simplay.model.AudioFile;
import com.finalproject.simplay.model.AudioFileExtras;
import com.finalproject.simplay.receiver.MyBroadcastReceiver;
import com.finalproject.simplay.repository.DatabaseHelper;
import com.finalproject.simplay.util.Helper;
import com.finalproject.simplay.configuration.PlayMode;
import com.finalproject.simplay.util.MyBuilder;

import java.util.ArrayList;

public class MusicService extends Service {

	private static final String DEBUG = Helper.setDebugTag("MusicService");

	public static final int FOREGROUND_ID = 666;
	public static final String NOTIFICATION_CHANNEL_NAME = "com.finalproject.simplay.notificationChannel";
	public static final String NOTIFICATION_CHANNEL_ID = "com.finalproject.simplay.notificationChannel01";

	private MyMediaPlayer mediaPlayer;
	private ArrayList<AudioFile> currentPlaylist;
	private int currentPlaylistPos;
	private float currentVolume = 1.0f;
	private AudioManager audioManager;
	private TelephonyManager telephonyManager;
	private final IBinder musicBinder = new MusicBinder();
	public final BroadcastReceiver broadcastReceiver = new MyBroadcastReceiver();
	public ComponentName receiverComponent;
	private DatabaseHelper dbHelper;

	private boolean serviceIsRunning = false;
	private boolean audioSupposedToBePlaying = false;
	private boolean audioPausedByTransientFocusLoss = false;
	private boolean rebuiltFromSnapshot = false;

	private String shuffleMode;
	private String repeatMode;

	private static final int FOCUSCHANGE = 0;
	private static final int FADEDOWN = 1;
	private static final int FADEUP = 2;



	/***************************************************
	 * Service Actions
	 ***************************************************/

	public static final String ACTION_START_NOTIFICATION = "com.finalproject.simplay.musicservice.ACTION_START_NOTIFICATION";
	public static final String ACTION_OUTPUT_DEVICE_CHANGED = "com.finalproject.simplay.musicservice.ACTION_OUTPUT_DEVICE_CHANGED";
	public static final String ACTION_PLAY_POSITION = "com.finalproject.simplay.musicservice.ACTION_PLAY_POSITION";
	public static final String ACTION_SEEK = "com.finalproject.simplay.musicservice.ACTION_SEEK";
	public static final String ACTION_PAUSE = "com.finalproject.simplay.musicservice.ACTION_PAUSE";
	public static final String ACTION_PREV = "com.finalproject.simplay.musicservice.ACTION_PREV";
	public static final String ACTION_NEXT = "com.finalproject.simplay.musicservice.ACTION_NEXT";



	/***************************************************
	 * Handlers, Listeners
	 ***************************************************/

	private final Handler mediaPlayerHandler = new Handler(Looper.myLooper()) {
		@Override
		public void handleMessage(@NonNull Message msg) {
			switch (msg.what) {
				case FADEDOWN:
					currentVolume -= 0.05f;
					if (currentVolume > 0.2f) {
						mediaPlayerHandler.sendEmptyMessageDelayed(FADEDOWN, 10);
					} else {
						currentVolume = 0.2f;
					}
					mediaPlayer.setVolume(currentVolume, currentVolume);
					break;

				case FADEUP:
					currentVolume += 0.01f;
					if (currentVolume < 1.0f) {
						mediaPlayerHandler.sendEmptyMessageDelayed(FADEUP, 10);
					} else {
						currentVolume = 1.0f;
					}
					break;

				case FOCUSCHANGE:
					switch (msg.arg1) {

						case AudioManager.AUDIOFOCUS_LOSS:
							if (audioSupposedToBePlaying) {
								audioPausedByTransientFocusLoss = false;
							}
							pauseHard();
							break;

						case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
							audioPausedByTransientFocusLoss = true;
							pauseHard();
							break;

						case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
							mediaPlayerHandler.removeMessages(FADEUP);
							mediaPlayerHandler.sendEmptyMessage(FADEDOWN);
							break;

						case AudioManager.AUDIOFOCUS_GAIN:
							if (audioPausedByTransientFocusLoss) {
								audioPausedByTransientFocusLoss = false;
							} else {
								mediaPlayerHandler.removeMessages(FADEDOWN);
								mediaPlayerHandler.sendEmptyMessage(FADEUP);
							}
					}
					break;

			}

			super.handleMessage(msg);
		}
	};

	private final AudioManager.OnAudioFocusChangeListener audioFocusChangeListener =
			focusChange -> mediaPlayerHandler
					.obtainMessage(FOCUSCHANGE, focusChange, 0)
					.sendToTarget();

	//	Since our app needs to run on API 28 (and ofc also due to backwards compatibility),
	//	we use PhoneStateListener even though it's been flagged deprecated.
	//	TelephonyCallback.CallStateListener would be it's replacement, but needs API 31.
	private final PhoneStateListener phoneStateListener = new PhoneStateListener() {
		@Override
		public void onCallStateChanged(int state, String phoneNumber) {
			switch (state) {
				case TelephonyManager.CALL_STATE_RINGING:
					if (audioSupposedToBePlaying) {
						audioPausedByTransientFocusLoss = true;
						pauseHard();
					}
					break;
				case TelephonyManager.CALL_STATE_OFFHOOK:
					audioPausedByTransientFocusLoss = false;
					if (audioSupposedToBePlaying) {
						pauseHard();
					}
					break;
				case TelephonyManager.CALL_STATE_IDLE:
					break;
			}
		}
	};



	/***************************************************
	 * Service Overrides
	 ***************************************************/

	@Override
	public void onCreate() {
		super.onCreate();

		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_MEDIA_BUTTON);
		filter.addAction(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
		filter.setPriority(1000000000);
		registerReceiver(broadcastReceiver, filter);

		telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		audioManager.requestAudioFocus(audioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
		telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);


		// Fallback for random bt control loss
		receiverComponent = new ComponentName(this, MyBroadcastReceiver.class);
		audioManager.registerMediaButtonEventReceiver(receiverComponent);

		currentPlaylist = new ArrayList<>();
		mediaPlayer = MyMediaPlayer.getInstance();

		mediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);

		dbHelper = DatabaseHelper.getInstance(this);
		mediaPlayer.setOnCompletionListener(view -> playNextPosition());

		// Setting/Updating notification content
		mediaPlayer.setOnPreparedListener(view -> {
			if (audioSupposedToBePlaying || rebuiltFromSnapshot) {
				startForeground(FOREGROUND_ID, notificationBuilder().build());
			} else {
				stopForeground(true);
			}
		});

		ContentValues playbackSettings = dbHelper.getContentValues(DatabaseHelper.PLAYBACK_SETTINGS_TABLE);
		shuffleMode = playbackSettings.get(PlayMode.MODE_SHUFFLE).toString();
		repeatMode = playbackSettings.get(PlayMode.MODE_REPEAT).toString();

		this.serviceIsRunning = true;
		Log.d(DEBUG, "Service created ...");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		boolean foregroundAlreadyStarted = false;
		switch (intent.getAction()) {

			case ACTION_START_NOTIFICATION:
				startForeground(FOREGROUND_ID, notificationBuilder().build());
				foregroundAlreadyStarted = true;
				break;

			case ACTION_OUTPUT_DEVICE_CHANGED:
				pauseHard();
				break;

			case ACTION_PLAY_POSITION:
				int newPosition = intent.getIntExtra("audioFileListPos", 0);
				setCurrentPlaylistPos(newPosition);
				play();
				break;

			case ACTION_PAUSE:
				pause();
				break;

			case ACTION_SEEK:
				int newSeekPosition = intent.getIntExtra("audioFileSeekPos", 0);
				seekTo(newSeekPosition);
				break;

			case ACTION_PREV:
				playPreviousPosition();
				break;

			case ACTION_NEXT:
				playNextPosition();
				break;
		}
		if (getCurrentAudioFile() != null && !foregroundAlreadyStarted) {
			startForeground(FOREGROUND_ID, notificationBuilder().build());
		}
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		if (!audioSupposedToBePlaying) {
			audioManager.abandonAudioFocus(audioFocusChangeListener);
			telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
			mediaPlayerHandler.removeCallbacksAndMessages(null);

			audioManager.unregisterMediaButtonEventReceiver(receiverComponent);
			unregisterReceiver(broadcastReceiver);
			this.serviceIsRunning = false;
			super.onDestroy();
			Log.d(DEBUG, "Service destroyed !");
/*
			mediaPlayer.release();
			mediaPlayer = null;
*/
		}
	}

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		Log.d(DEBUG, "Service bound ...");
		return musicBinder;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		Log.d(DEBUG, "Service unbound ...");
		return false;
	}

	@Override
	public void onRebind(Intent intent) {
		Log.d(DEBUG, "Service rebound ...");
		super.onRebind(intent);
	}



	/***************************************************
	 * Getters, Setters
	 ***************************************************/

	public boolean serviceIsRunning() {
		return serviceIsRunning;
	}

	public void prepareAndSeek(int position) {
		try {
			rebuiltFromSnapshot = true;
			AudioFile currentAudioFile = getCurrentAudioFile();
			Uri uri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, currentAudioFile.getId());
			mediaPlayer.reset();
			mediaPlayer.setDataSource(this, uri);
			mediaPlayer.prepare();
			mediaPlayer.seekTo(position);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public AudioFile getCurrentAudioFile() {
		if (getCurrentPlaylist() != null)
			return currentPlaylist.get(currentPlaylistPos);
		return null;
	}

	public AudioFileExtras getCurrentAudioFileDetails() {
		return null;
	}

	public void setCurrentPlaylist(ArrayList<AudioFile> playlist) {
		this.currentPlaylist = playlist;
		Log.d(DEBUG, "Playlist set, size: " + currentPlaylist.size());
	}

	public ArrayList<AudioFile> getCurrentPlaylist() {
		if (currentPlaylist.size() > 0) {
			return currentPlaylist;
		}
		return null;
	}

	public int getCurrentPlaylistPos() {
		return currentPlaylistPos;
	}

	public void playNext(AudioFile audioFile) {
		int nextPos = 0;
		ArrayList<AudioFile> musicList;
		if (getCurrentPlaylist() == null) {
			musicList = new ArrayList<>();
			musicList.add(audioFile);
			setCurrentPlaylist(musicList);
			setCurrentPlaylistPos(nextPos);
			play();
		} else {
			musicList = getCurrentPlaylist();
			musicList.add(getCurrentPlaylistPos() + 1, audioFile);
			setCurrentPlaylist(musicList);
		}
		preparePlayModesForPlayNext();
		MyBuilder.createToast(this, MyBuilder.TOAST_NOTIFICATION, "Song enqueued for playback.").show();
		dbHelper.takeSnapshot(currentPlaylist, getCurrentPlaylistPos(), getCurrentPosition());
	}

	private void preparePlayModesForPlayNext() {
		ContentValues playbackSettings = dbHelper.getContentValues(DatabaseHelper.PLAYBACK_SETTINGS_TABLE);

		if (playbackSettings.get(PlayMode.MODE_SHUFFLE).equals(PlayMode.MODE_ACTIVE)) {
			setShuffleMode(PlayMode.MODE_INACTIVE);
		}
		if (playbackSettings.get(PlayMode.MODE_REPEAT).equals(PlayMode.REPEAT_ONE)) {
			setRepeatMode(PlayMode.MODE_INACTIVE);
		}
	}

	public void setCurrentPlaylistPos(int currentPlaylistPos) {
		this.currentPlaylistPos = currentPlaylistPos;
	}

	public boolean isPlaying() {
		return audioSupposedToBePlaying;
	}

	public int getCurrentPosition() {
		return mediaPlayer.getCurrentPosition();
	}

	public int getDuration() {
		return mediaPlayer.getDuration();
	}



	/***************************************************
	 * Media player controls
	 ***************************************************/

	public void play() {
		if (telephonyManager.getCallState() == TelephonyManager.CALL_STATE_OFFHOOK) {
			return;
		}

		if (mediaPlayer.isPlaying()) {
			mediaPlayer.stop();
		}
		mediaPlayer.reset();

		try {
			AudioFile currentAudioFile = getCurrentAudioFile();
			Uri uri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, currentAudioFile.getId());
			mediaPlayer.setDataSource(this, uri);
			mediaPlayer.prepare();
			mediaPlayer.start();
			audioSupposedToBePlaying = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public synchronized void pause() {
		if (audioSupposedToBePlaying) {
			mediaPlayer.pause();
			audioSupposedToBePlaying = false;
		} else {
			mediaPlayer.start();
			audioSupposedToBePlaying = true;
		}
		remoteUpdatePlayerUI(true);
	}

	public synchronized void pauseHard() {
		mediaPlayer.pause();
		audioSupposedToBePlaying = false;
		remoteUpdatePlayerUI(true);
	}

	public void seekTo(int position) {
		mediaPlayer.seekTo(position);
	}

	public void playNextPosition() {
		int nextPosition;

		if (repeatMode.equals(PlayMode.REPEAT_ONE)) {
			nextPosition = currentPlaylistPos;
		} else if (shuffleMode.equals(PlayMode.MODE_ACTIVE)) {
			nextPosition = Helper.randBetween(0, currentPlaylist.size() - 1);
		} else if (currentPlaylistPos == currentPlaylist.size() - 1) {
			if (repeatMode.equals(PlayMode.REPEAT_ALL)) {
				nextPosition = 0;
			} else {
				nextPosition = -1;
				MyBuilder.createToast(this, MyBuilder.TOAST_NOTIFICATION, "End of playlist reached.").show();
			}
		} else {
			nextPosition = currentPlaylistPos + 1;
		}

		if (nextPosition >= 0) {
			setCurrentPlaylistPos(nextPosition);
			play();
			remoteUpdatePlayerUI(false);
		}
	}

	public void playPreviousPosition() {
		int previousPosition;

		if (mediaPlayer.getCurrentPosition() < 1000
				&& !repeatMode.equals(PlayMode.REPEAT_ONE)
		) {
			if (currentPlaylistPos == 0) {
				if (repeatMode.equals(PlayMode.REPEAT_ALL)) {
					previousPosition = currentPlaylist.size() - 1;
				} else {
					previousPosition = -1;
					MyBuilder.createToast(this, MyBuilder.TOAST_NOTIFICATION, "Can't go back any further.").show();
				}
			} else {
				previousPosition = currentPlaylistPos - 1;
			}
		} else {
			previousPosition = currentPlaylistPos;
		}

		if (previousPosition >= 0) {
			setCurrentPlaylistPos(previousPosition);
			play();
			remoteUpdatePlayerUI(false);
		}
	}

	public void setShuffleMode(String mode) {
		this.shuffleMode = mode;
		dbHelper.setShuffleMode(mode);
		MyBuilder.createToast(this, MyBuilder.TOAST_SYSTEM, "Shuffle " + PlayMode.print(mode)).show();
	}

	public void setRepeatMode(String mode) {
		this.repeatMode = mode;
		dbHelper.setRepeatMode(mode);
		MyBuilder.createToast(this, MyBuilder.TOAST_SYSTEM, "Repeat " + PlayMode.print(mode)).show();
	}

	private void remoteUpdatePlayerUI(boolean updateButtonStatesOnly) {
		if (PlayerActivity.getInstanceActivity() != null) {
			if (!updateButtonStatesOnly) {
				PlayerActivity.getInstanceActivity().updateCurrentAudioFile(getCurrentAudioFile());
			}
			PlayerActivity.getInstanceActivity().updateBtnPlayPauseState();
		}
	}



	/***************************************************
	 * Inner Classes
	 ***************************************************/

	public class MusicBinder extends Binder {
		public MusicService getService() {
			return MusicService.this;
		}
	}



	/***************************************************
	 * Notification
	 ***************************************************/

	private NotificationCompat.Builder notificationBuilder() {
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		RemoteViews notificationLayout = new RemoteViews(getPackageName(), R.layout.notification);
		AudioFile audioFile = getCurrentAudioFile();
		String songName = audioFile.getTitle();
		String songArtist = audioFile.getArtist();

		PendingIntent pi = PendingIntent.getActivity(
				getApplicationContext(),
				0,
				new Intent(getApplicationContext(), CategoryActivity.class),
				PendingIntent.FLAG_IMMUTABLE);

		NotificationChannel notificationChannel =
				new NotificationChannel(
						NOTIFICATION_CHANNEL_ID,
						NOTIFICATION_CHANNEL_NAME,
						NotificationManager.IMPORTANCE_HIGH);
		notificationChannel.enableVibration(false);
		notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
		notificationManager.createNotificationChannel(notificationChannel);

		Bitmap coverArt = Helper.extractAlbumArt(getCurrentAudioFile().getDataPath());
		if (coverArt == null) {
			coverArt = BitmapFactory.decodeResource(getResources(), R.drawable.album_art_blank);
		}

		int playPauseDrawable = isPlaying()
				? R.drawable.ic_notification_pause
				: R.drawable.ic_notification_play;

		notificationLayout.setImageViewResource(R.id.btn_notification_play, playPauseDrawable);
		notificationLayout.setImageViewBitmap(R.id.notification_album_art, coverArt);
		notificationLayout.setTextViewText(R.id.notification_title, Helper.clampString(songName, Helper.STRLEN_NOTIFICATION));
		notificationLayout.setTextViewText(R.id.notification_artist, Helper.clampString(songArtist, Helper.STRLEN_NOTIFICATION));

		Intent playPauseIntent = new Intent(getApplicationContext(), MusicService.class).setAction(ACTION_PAUSE);
		Intent playPrevIntent = new Intent(getApplicationContext(), MusicService.class).setAction(ACTION_PREV);
		Intent playNextIntent = new Intent(getApplicationContext(), MusicService.class).setAction(ACTION_NEXT);

		PendingIntent playPausePI = PendingIntent.getService(getBaseContext(), 0, playPauseIntent, PendingIntent.FLAG_IMMUTABLE);
		PendingIntent playPrevPI = PendingIntent.getService(getBaseContext(), 0, playPrevIntent, PendingIntent.FLAG_IMMUTABLE);
		PendingIntent playNextPI = PendingIntent.getService(getBaseContext(), 0, playNextIntent, PendingIntent.FLAG_IMMUTABLE);

		notificationLayout.setOnClickPendingIntent(R.id.btn_notification_prev, playPrevPI);
		notificationLayout.setOnClickPendingIntent(R.id.btn_notification_play, playPausePI);
		notificationLayout.setOnClickPendingIntent(R.id.btn_notification_next, playNextPI);

		return new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID)
				.setSmallIcon(R.mipmap.ic_status_bar)
				.setCustomContentView(notificationLayout)
				.setOngoing(true)
				.setSilent(true)
				.setAutoCancel(true)
				.setContentIntent(pi);
	}

}
