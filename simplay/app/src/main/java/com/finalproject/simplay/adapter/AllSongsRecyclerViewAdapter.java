package com.finalproject.simplay.adapter;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.simplay.R;
import com.finalproject.simplay.model.AudioFile;
import com.finalproject.simplay.service.MusicService;
import com.finalproject.simplay.util.MyBuilder;
import com.finalproject.simplay.util.Formatter;
import com.finalproject.simplay.util.Helper;
import com.turingtechnologies.materialscrollbar.IDateableAdapter;
import com.turingtechnologies.materialscrollbar.INameableAdapter;

import java.util.ArrayList;

public class AllSongsRecyclerViewAdapter extends RecyclerView.Adapter<AllSongsRecyclerViewAdapter.AllSongsViewHolder>
implements INameableAdapter {

    private static final String DEBUG = Helper.setDebugTag("AllSongsRecyclerViewAdapter");

    private ArrayList<AudioFile> allSongList;
    private MusicService musicService;


    private final ServiceConnection musicServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            MusicService.MusicBinder musicBinder = (MusicService.MusicBinder) iBinder;
            musicService = musicBinder.getService();
            Log.d(DEBUG, "Service connected ...");
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(DEBUG, "Service disconnected ...");
        }
    };

    public void setAllSongList(ArrayList<AudioFile> allSongList) {
        this.allSongList = allSongList;
    }

    @NonNull
    @Override
    public AllSongsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Intent musicServiceIntent = new Intent(parent.getContext(), MusicService.class);
        parent.getContext().bindService(musicServiceIntent, musicServiceConnection, Context.BIND_AUTO_CREATE);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_audio_file, parent, false);
        return new AllSongsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AllSongsViewHolder holder, int position) {
        AudioFile currentItem = allSongList.get(position);
        String durationFormatted = Formatter.msToMinutesAndSeconds(currentItem.getDuration(), false);

        holder.songArtist.setText(currentItem.getArtist());
        holder.songTitle.setText(currentItem.getTitle());
        holder.songArtist.setSingleLine(true);
        holder.songTitle.setSingleLine(true);
        holder.songDuration.setText(durationFormatted);

        holder.kebapMenuButton.setOnClickListener(kebapMenuButtonClicked -> {
            PopupMenu popupMenu = MyBuilder.createGenericPopup(holder.itemView.getContext(), holder.kebapMenuButton);
            popupMenu.getMenuInflater().inflate(R.menu.menu_dropdown, popupMenu.getMenu());
            popupMenu.getMenu().add("Add to Playlist");
            popupMenu.getMenu().add("Play Next");
            popupMenu.setOnMenuItemClickListener(menuItemClicked -> {
                if (menuItemClicked.getTitle().equals("Add to Playlist")) {
                    AlertDialog dialog = MyBuilder.createAddToPlaylistDialog(holder.itemView, currentItem,"AllSongs");
                    dialog.show();
                } else if (menuItemClicked.getTitle().equals("Play Next")) {
                    musicService.playNext(currentItem);
                }
                return false;
            });
            popupMenu.show();
        });

        holder.itemView.setOnClickListener(view -> {
            musicService.setCurrentPlaylist(allSongList);

            Intent musicServiceIntent = new Intent(view.getContext(), MusicService.class);
            musicServiceIntent.putExtra("audioFileListPos", position);
            musicServiceIntent.setAction(MusicService.ACTION_PLAY_POSITION);
            view.getContext().startService(musicServiceIntent);

            Navigation.findNavController(holder.itemView).navigate(R.id.action_viewPagerFragmentContainer_to_playerActivity2);
        });
    }


    @Override
    public int getItemCount() {
        return allSongList.size();
    }

    @Override
    public Character getCharacterForElement(int element) {
        char c = allSongList.get(element).getArtist().charAt(0);
        if (Character.isDigit(c)) {
            c = '#';
        }
        return c;
    }

    public static class AllSongsViewHolder extends RecyclerView.ViewHolder {

        private final TextView songTitle;
        private final TextView songArtist;
        private final TextView songDuration;
        private final AppCompatImageButton kebapMenuButton;

        public AllSongsViewHolder(@NonNull View itemView) {
            super(itemView);
            songTitle = itemView.findViewById(R.id.txtAudioTitle);
            songArtist = itemView.findViewById(R.id.txtAudioArtist);
            songDuration = itemView.findViewById(R.id.txtAudioDuration);
            kebapMenuButton = itemView.findViewById(R.id.dropDownToggleSongItems);
        }

    }

}
