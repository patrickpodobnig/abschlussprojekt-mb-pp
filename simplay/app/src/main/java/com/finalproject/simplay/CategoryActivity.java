package com.finalproject.simplay;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.fragment.NavHostFragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.finalproject.simplay.model.AudioFile;
import com.finalproject.simplay.service.MusicService;
import com.finalproject.simplay.repository.DatabaseHelper;
import com.finalproject.simplay.util.Helper;
import com.finalproject.simplay.util.MyBuilder;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;


public class CategoryActivity extends AppCompatActivity
		implements NavigationView.OnNavigationItemSelectedListener {

	private static final String DEBUG = Helper.setDebugTag("CategoryActivity");

	private MusicService musicService;
	private boolean musicServiceBound = false;

	public DrawerLayout drawerLayout;
	public ActionBarDrawerToggle actionBarDrawerToggle;

	//Checks if permission to read external storage is granted
	private final ActivityResultLauncher<String> requestPermissionLauncher =
			registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
				if (!isGranted) {
					MyBuilder.createToast(this, MyBuilder.TOAST_SYSTEM, "You need to grant permission in order to use this app.").show();
				}
			});


	private final ServiceConnection musicServiceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
			MusicService.MusicBinder musicBinder = (MusicService.MusicBinder) iBinder;
			musicService = musicBinder.getService();
			musicServiceBound = true;
			Log.d(DEBUG, "Service connected ...");

			if (musicService.getCurrentAudioFile() == null) {
				Log.d(DEBUG, "No playlist found, retrieving snapshot...");
				DatabaseHelper dbHelper = DatabaseHelper.getInstance(getApplicationContext());
				ArrayList<AudioFile> playlistSnapshot = dbHelper.retrievePlaylistSnapshot();

				if (playlistSnapshot != null) {
					ContentValues playbackSnapshot = dbHelper.retrievePlaybackSnapshot();
					musicService.setCurrentPlaylist(playlistSnapshot);
					int snapshotPlaylistPos = playbackSnapshot.getAsInteger(DatabaseHelper.SNAPSHOT_PLAYLIST_POS);
					musicService.setCurrentPlaylistPos(snapshotPlaylistPos);
					musicService.prepareAndSeek(playbackSnapshot.getAsInteger(DatabaseHelper.SNAPSHOT_PLAYBACK_POS));
					Log.d(DEBUG, "... snapshot retrieved !");
				} else {
					Log.d(DEBUG, "... snapshot not found !");
				}
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			musicServiceBound = false;
			Log.d(DEBUG, "Service disconnected ...");
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_category_activity, menu);
		return true;
	}

	// [ACTIONBAR]
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
			case R.id.miPlayer:
				if (musicService.getCurrentAudioFile() != null) {
					startActivity(new Intent(this, PlayerActivity.class));
				} else {
					MyBuilder.createToast(this, MyBuilder.TOAST_NOTIFICATION, "No active playlist found.");
				}
				return true;
			default:
				if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
					return true;
				}
				return super.onOptionsItemSelected(item);
		}
	}

	// Navigation Drawer
	@Override
	public boolean onNavigationItemSelected(@NonNull MenuItem item) {
		AlertDialog.Builder dialogBuilder = MyBuilder.createGenericDialogBuilder(this);
		TextView textView = new TextView(this);
		textView.setPadding(100, 0, 100, 0);

		switch (item.getItemId()) {
			case R.id.nav_info:
				dialogBuilder.setTitle("App version " + getString(R.string.app_version));
				textView.setText(R.string.app_information);
				break;
			case R.id.nav_about:
				dialogBuilder.setTitle("Designed and developed by:");
				textView.setText(R.string.app_authors);
				break;
			case R.id.nav_privacy:
				dialogBuilder.setTitle("Privacy Policy");
				textView.setText(R.string.app_privacy);
				textView.setMovementMethod(LinkMovementMethod.getInstance());
				break;
		}

		dialogBuilder.setView(textView);
		dialogBuilder.show();
		drawerLayout.closeDrawer(GravityCompat.START);
		return true;
	}

	@Override
	protected void onStart() {
		super.onStart();
		Intent musicServiceIntent = new Intent(this, MusicService.class);
		bindService(musicServiceIntent, musicServiceConnection, BIND_AUTO_CREATE);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (musicService.getCurrentPlaylist() != null) {
			DatabaseHelper dbHelper = DatabaseHelper.getInstance(this);
			dbHelper.takeSnapshot(musicService.getCurrentPlaylist(), musicService.getCurrentPlaylistPos(), musicService.getCurrentPosition());
			Log.d(DEBUG, "Playback snapshot taken ...");
		}


		if (!musicService.isPlaying()) {
			unbindService(musicServiceConnection);
			Log.d(DEBUG, "Service unbound ...");
			musicServiceBound = false;
			if (stopService(new Intent(this, MusicService.class))) {
				Log.d(DEBUG, "Service stopped !");
			}
		} else {
			Intent musicServiceIntent = new Intent(this, MusicService.class);
			musicServiceIntent.setAction(MusicService.ACTION_START_NOTIFICATION);
			startService(musicServiceIntent);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_category);

		//Checks if permission to read external storage is granted, if not, launches the requestPermissionLauncher
		if (
				ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
						!= PackageManager.PERMISSION_GRANTED
						|| ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WAKE_LOCK)
						!= PackageManager.PERMISSION_GRANTED
		) {
			requestPermissionLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE);
			requestPermissionLauncher.launch(Manifest.permission.WAKE_LOCK);
		}

		//NavHost Fragment which is connected to the navigationgraph in res/navigation
		NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
		FragmentManager navHostChildFragment = navHostFragment.getChildFragmentManager();

		// to make the Navigation drawer icon always appear on the action bar
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		drawerLayout = findViewById(R.id.drawer_layout);
		actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.nav_open, R.string.nav_close);

		// pass the Open and Close toggle for the drawer layout listener
		// to toggle the button
		drawerLayout.addDrawerListener(actionBarDrawerToggle);
		actionBarDrawerToggle.syncState();

		NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_drawer_view);
		navigationView.setNavigationItemSelectedListener(this);
/*
        int backStackEntryCount = navHostChildFragment.getBackStackEntryCount();
        List<Fragment> fragments = navHostChildFragment.getFragments();
        int fragmentCount = fragments.size();
*/

	}
}