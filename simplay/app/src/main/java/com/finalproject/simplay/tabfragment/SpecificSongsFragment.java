package com.finalproject.simplay.tabfragment;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.finalproject.simplay.R;

import com.finalproject.simplay.adapter.SpecificSongsRecViewAdapter;
import com.finalproject.simplay.model.AudioFile;
import com.finalproject.simplay.repository.MediaStoreHelper;
import com.finalproject.simplay.util.Helper;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.turingtechnologies.materialscrollbar.AlphabetIndicator;
import com.turingtechnologies.materialscrollbar.DragScrollBar;

import java.util.ArrayList;


public class SpecificSongsFragment extends Fragment {

	private SpecificSongsRecViewAdapter adapter;
	private RecyclerView recyclerView;
	private ArrayList<AudioFile> audioFiles;
	private String origin;
	private String filterString;

	@Override
	public void onResume() {
		requireActivity().invalidateOptionsMenu();
		super.onResume();
	}

	@Override
	public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		MenuItem menuItem = menu.findItem(R.id.action_search);
		menuItem.setVisible(true);

		// Fix to prevent search icon from collapsing into an empty kebap menu when exiting the searchbar
		// Still does if user switches tabs while search bar is focused, then exits it
		menuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
			@Override
			public boolean onMenuItemActionExpand(MenuItem menuItem) {
				return true;
			}

			@Override
			public boolean onMenuItemActionCollapse(MenuItem menuItem) {
				requireActivity().invalidateOptionsMenu();
				return true;
			}
		});

		SearchView searchView = (SearchView) menuItem.getActionView();
		searchView.setQuery("", true);
		searchView.clearFocus();

		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {
				return false;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				ArrayList<AudioFile> tempAudioList = new ArrayList<>();
				String scrollBarIndexSource = "Genre";
				if (origin.equals("Artist")) {
					for (AudioFile audioFile : audioFiles) {
						if (audioFile.getTitle().toUpperCase().contains(newText.toUpperCase())) {
							tempAudioList.add(audioFile);
						}
						scrollBarIndexSource = "Artist";
					}
				} else {
					for (AudioFile audioFile : audioFiles) {
						if (audioFile.getTitle().toUpperCase().contains(newText.toUpperCase())
								|| audioFile.getArtist().toUpperCase().contains(newText.toUpperCase())) {
							tempAudioList.add(audioFile);
						}
					}
				}
				adapter.setSpecificSongList(tempAudioList, scrollBarIndexSource);
				recyclerView.setAdapter(adapter);

				return false;
			}
		});
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Bundle args;
		View view = inflater.inflate(R.layout.fragment_specific_songs, container, false);
		setHasOptionsMenu(true);
		//receiving filterString from Bundle
		args = this.getArguments();
		filterString = args.getString("filterString");
		origin = args.getString("origin");
		long genreId = args.getLong("genreId");


		//initialising RecyclerView and respective ArrayList
		recyclerView = view.findViewById(R.id.specific_songs_recycler_view);
		audioFiles = new ArrayList<>();
		String scrollbarIndexSource = "Genre";

		MediaStoreHelper mediaStoreHelper = new MediaStoreHelper(requireActivity().getApplicationContext());
		Uri collection;

		if (origin.equals("Artist")) {
			audioFiles = mediaStoreHelper.getAudioFiles(MediaStoreHelper.FILTER_ARTIST, filterString);
			scrollbarIndexSource = "Artist";
		} else {
			audioFiles = mediaStoreHelper.getAudioFiles(MediaStoreHelper.FILTER_GENRE, genreId);

		}

		//setting up the Adapter and LayoutManager
		adapter = new SpecificSongsRecViewAdapter();
		adapter.setSpecificSongList(audioFiles, scrollbarIndexSource);
		recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
		recyclerView.setAdapter(adapter);

		TextView titleBarText = view.findViewById(R.id.title_bar_text);
		titleBarText.setText(filterString);
		titleBarText.setSingleLine(true);
		AppCompatImageButton btnNavBack = view.findViewById(R.id.btn_nav_back);
		btnNavBack.setOnClickListener(btnNavBackClicked -> Navigation.findNavController(btnNavBackClicked).popBackStack());

		DragScrollBar scrollBar = view.findViewById(R.id.scrollBar);
		scrollBar.setRecyclerView(recyclerView);
		scrollBar.setIndicator(new AlphabetIndicator(view.getContext()), false);

		return view;
	}

}
