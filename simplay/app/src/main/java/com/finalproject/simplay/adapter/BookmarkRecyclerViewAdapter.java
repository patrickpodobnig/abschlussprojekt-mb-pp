package com.finalproject.simplay.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.simplay.R;
import com.finalproject.simplay.model.Bookmark;
import com.finalproject.simplay.service.MusicService;
import com.finalproject.simplay.repository.DatabaseHelper;
import com.finalproject.simplay.util.Formatter;
import com.finalproject.simplay.util.Helper;

import java.util.ArrayList;

public class BookmarkRecyclerViewAdapter extends RecyclerView.Adapter<BookmarkRecyclerViewAdapter.BookmarkViewHolder> {
	private ArrayList<Bookmark> bookmarkList = new ArrayList<>();
	private int songLengthInMs;

	public void setBookmarkList(ArrayList<Bookmark> bookmarkList) {
		this.bookmarkList = bookmarkList;
		notifyDataSetChanged();
	}

	@NonNull
	@Override
	public BookmarkViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_bookmark, parent, false);
		return new BookmarkViewHolder(view);
	}


	@SuppressLint("NotifyDataSetChanged")
	@Override
	public void onBindViewHolder(@NonNull BookmarkViewHolder holder, int position) {
		Bookmark currentBookmark = bookmarkList.get(position);
		String formattedPosition = Formatter.msToMinutesAndSeconds(currentBookmark.getPosition(), songLengthInMs);
		holder.bookmarkPosition.setText(formattedPosition);
		holder.bookmarkTitle.setText(currentBookmark.getTitle());
		holder.bookmarkTitle.setSingleLine(true);

		holder.itemView.setOnClickListener(view -> {
			Intent intent = new Intent(view.getContext(), MusicService.class);
			intent.putExtra("audioFileSeekPos", currentBookmark.getPosition());
			intent.setAction(MusicService.ACTION_SEEK);
			view.getContext().startService(intent);
		});
		holder.delButton.setOnClickListener(delButtonClicked -> {
			DatabaseHelper databaseHelper = DatabaseHelper.getInstance(delButtonClicked.getContext());
			databaseHelper.deleteBookmark(currentBookmark.getBookmarkId());
			databaseHelper.close();
			ArrayList<Bookmark> newList = databaseHelper.getBookmarkList(currentBookmark.getSongId());
			setBookmarkList(newList);
			notifyDataSetChanged();
		});
	}

	@Override
	public int getItemCount() {
		return bookmarkList.size();
	}

	public void setSongLengthInMs(int songLengthInMs) {
		this.songLengthInMs = songLengthInMs;
	}

	public static class BookmarkViewHolder extends RecyclerView.ViewHolder {

		private TextView bookmarkTitle;
		private TextView bookmarkPosition;
		private ImageButton delButton;

		public BookmarkViewHolder(@NonNull View itemView) {
			super(itemView);
			delButton = itemView.findViewById(R.id.deleteBookmark);
			bookmarkTitle = itemView.findViewById(R.id.bookmark_title);
			bookmarkPosition = itemView.findViewById(R.id.bookmark_position);
		}
	}
}
