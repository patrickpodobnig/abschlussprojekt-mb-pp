package com.finalproject.simplay.adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.simplay.PlayerActivity;
import com.finalproject.simplay.R;
import com.finalproject.simplay.model.AudioFile;
import com.finalproject.simplay.service.MusicService;
import com.finalproject.simplay.util.MyBuilder;
import com.finalproject.simplay.util.Formatter;
import com.finalproject.simplay.util.Helper;
import com.turingtechnologies.materialscrollbar.INameableAdapter;

import java.util.List;

public class CurrentPlaylistRecViewAdapter extends RecyclerView.Adapter<CurrentPlaylistRecViewAdapter.ViewHolder>
	implements INameableAdapter {

	private static final String DEBUG = Helper.setDebugTag("CurrentPlaylistRecViewAdapter");

	private List<AudioFile> audioFileList;
	private int selectedPosition = RecyclerView.NO_POSITION;

	private MusicService musicService;


	private final ServiceConnection musicServiceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
			MusicService.MusicBinder musicBinder = (MusicService.MusicBinder) iBinder;
			musicService = musicBinder.getService();
			Log.d(DEBUG, "service connected");
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			Log.d(DEBUG, "service disconnected");
		}
	};

	public CurrentPlaylistRecViewAdapter(List<AudioFile> audioFileList) {
		this.audioFileList = audioFileList;
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		Intent musicServiceIntent = new Intent(parent.getContext(), MusicService.class);
		parent.getContext().bindService(musicServiceIntent, musicServiceConnection, Context.BIND_AUTO_CREATE);
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_audio_file, parent, false);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
		AudioFile currentItem = audioFileList.get(position);
		String durationFormatted = Formatter.msToMinutesAndSeconds(currentItem.getDuration(), false);

		holder.txtAudioArtist.setText(currentItem.getArtist());
		holder.txtAudioTitle.setText(currentItem.getTitle());
		holder.txtAudioArtist.setSingleLine(true);
		holder.txtAudioTitle.setSingleLine(true);
		holder.txtAudioDuration.setText(durationFormatted);

		holder.audioListParent.setOnClickListener(view -> {
			Intent musicServiceIntent = new Intent(view.getContext(), MusicService.class);
			musicServiceIntent.putExtra("audioFileListPos", position);
			musicServiceIntent.setAction(MusicService.ACTION_PLAY_POSITION);
			view.getContext().startService(musicServiceIntent);

			if (PlayerActivity.getInstanceActivity() != null) {
				PlayerActivity.getInstanceActivity().updateCurrentAudioFile(currentItem);
			}

			notifyItemChanged(selectedPosition);
			this.selectedPosition = holder.getAdapterPosition();
			notifyItemChanged(selectedPosition);
		});

		holder.kebapMenuButton.setOnClickListener(kebapMenuButtonClicked -> {
			PopupMenu popupMenu = MyBuilder.createGenericPopup(holder.itemView.getContext(), holder.kebapMenuButton);
			popupMenu.getMenuInflater().inflate(R.menu.menu_dropdown, popupMenu.getMenu());
			popupMenu.getMenu().add("Add to Playlist");
			popupMenu.getMenu().add("Play Next");
			popupMenu.setOnMenuItemClickListener(menuItemClicked -> {
				if (menuItemClicked.getTitle().equals("Add to Playlist")) {
					AlertDialog dialog = MyBuilder.createAddToPlaylistDialog(holder.itemView, currentItem,"CurrentSongs");
					dialog.show();
				} else if (menuItemClicked.getTitle().equals("Play Next")) {
					musicService.playNext(currentItem);
					setAudioFileList(musicService.getCurrentPlaylist());
				}
				return false;
			});
			popupMenu.show();
		});
		holder.itemView.setSelected(selectedPosition == position);
	}

	@Override
	public int getItemCount() {
		return audioFileList.size();
	}

	public void setSelectedPosition(int position) {
		notifyItemChanged(selectedPosition);
		this.selectedPosition = position;
		notifyItemChanged(selectedPosition);
	}

	@SuppressLint("NotifyDataSetChanged")
	public void setAudioFileList(List<AudioFile> audioFileList) {
		this.audioFileList = audioFileList;
		notifyDataSetChanged();
	}

	@Override
	public Character getCharacterForElement(int element) {
		char c = audioFileList.get(element).getArtist().charAt(0);
		if (Character.isDigit(c)) {
			c = '#';
		}
		return c;
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {

		private final TextView txtAudioArtist, txtAudioTitle, txtAudioDuration;
		private final LinearLayout audioListParent;
		private final AppCompatImageButton kebapMenuButton;

		public ViewHolder(@NonNull View itemView) {
			super(itemView);
			audioListParent = itemView.findViewById(R.id.audioListParent);
			txtAudioArtist = itemView.findViewById(R.id.txtAudioArtist);
			txtAudioTitle = itemView.findViewById(R.id.txtAudioTitle);
			txtAudioDuration = itemView.findViewById(R.id.txtAudioDuration);
			kebapMenuButton = itemView.findViewById(R.id.dropDownToggleSongItems);
		}
	}
}
