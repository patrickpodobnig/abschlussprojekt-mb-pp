package com.finalproject.simplay.model;

public class Playlist {

    private final String playlistTableName;
    private final String playlistTitle;

    public Playlist(String playlistTableName, String playlistTitle) {
        this.playlistTableName = playlistTableName;
        this.playlistTitle = playlistTitle;
    }

    public String getPlaylistTableName() {
        return playlistTableName;
    }

    public String getPlaylistTitle() {
        return playlistTitle;
    }
}
