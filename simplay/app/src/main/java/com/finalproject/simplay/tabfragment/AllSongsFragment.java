package com.finalproject.simplay.tabfragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.finalproject.simplay.R;
import com.finalproject.simplay.adapter.AllSongsRecyclerViewAdapter;
import com.finalproject.simplay.model.AudioFile;
import com.finalproject.simplay.repository.MediaStoreHelper;
import com.finalproject.simplay.util.Helper;
import com.turingtechnologies.materialscrollbar.AlphabetIndicator;
import com.turingtechnologies.materialscrollbar.DragScrollBar;
import com.turingtechnologies.materialscrollbar.TouchScrollBar;

import java.util.ArrayList;

public class AllSongsFragment extends Fragment {

	private static final String DEBUG = Helper.setDebugTag("AllSongsFragment");

	private ArrayList<AudioFile> audioFiles;
	AllSongsRecyclerViewAdapter adapter;
	private RecyclerView recyclerView;


	@Override
	public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		MenuItem menuItem = menu.findItem(R.id.action_search);
		//menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		menuItem.setVisible(true);
		SearchView searchView = (SearchView) menuItem.getActionView();

		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {
				return false;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				ArrayList<AudioFile> tempAudioList = new ArrayList<>();
				if (newText.equals("")) {
					adapter.setAllSongList(audioFiles);
				} else {
					for (AudioFile audioFile : audioFiles) {
						if (audioFile.getTitle().toUpperCase().contains(newText.toUpperCase())
								|| audioFile.getArtist().toUpperCase().contains(newText.toUpperCase())) {
							tempAudioList.add(audioFile);
						}
					}
					adapter.setAllSongList(tempAudioList);
				}
				recyclerView.setAdapter(adapter);
				return false;
			}
		});
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_all_songs, container, false);
		setHasOptionsMenu(true);

		//initialising RecyclerView and respective ArrayList
		recyclerView = view.findViewById(R.id.all_songs_recycler_view);
		audioFiles = new ArrayList<>();

		MediaStoreHelper mediaStoreHelper = new MediaStoreHelper(requireActivity().getApplicationContext());
		audioFiles = mediaStoreHelper.getAudioFiles();

		//setting up the Adapter and LayoutManager
		adapter = new AllSongsRecyclerViewAdapter();
		adapter.setAllSongList(audioFiles);
		recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
		recyclerView.setAdapter(adapter);

		DragScrollBar scrollBar = view.findViewById(R.id.scrollBar);
		scrollBar.setRecyclerView(recyclerView);
		scrollBar.setIndicator(new AlphabetIndicator(view.getContext()), false);

		return view;
	}

}
