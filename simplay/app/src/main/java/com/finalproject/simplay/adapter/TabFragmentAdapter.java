package com.finalproject.simplay.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.finalproject.simplay.tabfragment.AllSongsFragment;
import com.finalproject.simplay.tabfragment.ArtistFragment;
import com.finalproject.simplay.tabfragment.GenreFragment;
import com.finalproject.simplay.tabfragment.PlaylistFragment;

public class TabFragmentAdapter extends FragmentStateAdapter {
    public TabFragmentAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    //Creates Fragment based on the position of the current Tab
    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 1:
                return new GenreFragment();
            case 2:
                return new ArtistFragment();
            case 3:
                return new AllSongsFragment();
        }
        return new PlaylistFragment();
    }

    //returns number of tabs/tabfragments
    @Override
    public int getItemCount() {
        return 4;
    }
}
