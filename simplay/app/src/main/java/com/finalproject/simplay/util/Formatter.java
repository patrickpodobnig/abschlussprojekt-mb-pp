package com.finalproject.simplay.util;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

@SuppressLint("DefaultLocale")
public class Formatter {


	/**
	 * Returns a quoted string, having it's own quotes escaped.
	 * @param string: e.g. string "quoted"
	 * @return "string \"quoted\""
	 */

	public static String prepareDblQuotedString(String string) {
		return "\"" + string.replace("\"", "\"\"") + "\"";
	}

	public static String prepareDblQuotedString(String string, boolean doNotEscape) {
		return "\"" + string + "\"";
	}



	/**
	 * Converts bytes to megabytes and suffixes " MB"
	 * @param bytes: passed bytes
	 * @return "n.00 MB"
	 */

	public static String bytesToMegabytes(long bytes) {
		int factor = 1_048_576;
		double megabytes = (double) bytes / factor;
		return String.format(Locale.GERMAN, "%.2f MB", megabytes);
	}


	/**
	 * Converts a UNIX timestamp (in seconds!) to a date string
	 * @param timestamp: passed timestamp in seconds
	 * @return YYYY/MM/DD HH:MM:SS
	 */

	public static String unixTimeToReadable(long timestamp) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd H:mm:ss", Locale.GERMAN);
		return simpleDateFormat.format(timestamp * 1000); // Expects milliseconds
	}



	/**
	 * @param duration: time passed in MS
	 * @param showLeadingZeros: flag to determine if leading zeros should be shown
	 * @return Duration formatted to (HH:)MM:SS
	 */

	public static String msToMinutesAndSeconds(int duration, boolean showLeadingZeros) {
		String patternPrefix = showLeadingZeros ? "%02d" : "%d";
		String durationFormatted;
		long secondsSplit = TimeUnit.MILLISECONDS.toSeconds(duration) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration));

		if (duration < 1000 * 60 * 60) {
			durationFormatted = String.format(patternPrefix + ":%02d",
					TimeUnit.MILLISECONDS.toMinutes(duration),
					secondsSplit
			);
		} else {
			durationFormatted = String.format(patternPrefix + ":%02d:%02d",
					TimeUnit.MILLISECONDS.toHours(duration),
					TimeUnit.MILLISECONDS.toMinutes(duration) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(duration)),
					secondsSplit
			);
		}

		return durationFormatted;
	}



	/**
	 * Overload to show full-sized placeholders for time passed (e.g. 00:00:01 if the track lasts over 1h)
	 * so the UI elements won't shift during playback (59:59 > 1:00:00 results in more occupied space)
	 * @param duration: time passed in MS
	 * @param totalLength: time total in MS
	 * @return Duration formatted to (HH:)MM:SS
	 */

	public static String msToMinutesAndSeconds(int duration, int totalLength) {
		String durationFormatted;
		long secondsSplit = TimeUnit.MILLISECONDS.toSeconds(duration) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration));

		if (totalLength < 1000 * 60 * 60) {
			durationFormatted = String.format("%02d:%02d",
					TimeUnit.MILLISECONDS.toMinutes(duration),
					secondsSplit
			);
		} else {
			durationFormatted = String.format("%02d:%02d:%02d",
					TimeUnit.MILLISECONDS.toHours(duration),
					TimeUnit.MILLISECONDS.toMinutes(duration) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(duration)),
					secondsSplit
			);
		}

		return durationFormatted;
	}

}
