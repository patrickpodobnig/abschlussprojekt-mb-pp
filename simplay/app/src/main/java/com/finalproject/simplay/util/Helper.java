package com.finalproject.simplay.util;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.Locale;

public class Helper {

	public static final int STRLEN_LIST_ITEM_GENERIC = 55;
	public static final int STRLEN_NOTIFICATION = 40;
	public static final int STRLEN_AUDIOLIST = 42;
	public static final int STRLEN_BOOKMARKLIST = 25;


	public static boolean deviceIsInLandscape(Context calledByContext) {
		int orientation = calledByContext.getResources().getConfiguration().orientation;
		return orientation == Configuration.ORIENTATION_LANDSCAPE;
	}


	/**
	 * Returns a String used for Log.d() debug tags
	 *
	 * @param className: reference to the class Log.d() is called from
	 * @return String
	 */
	public static String setDebugTag(String className) {
		return String.format("***** [DEBUG %20s] *****", className);
	}


	/**
	 * Returns a random number between min and max
	 *
	 * @param min: lower boundary
	 * @param max: upper boundary
	 * @return Integer
	 */
	public static int randBetween(int min, int max) {
		return (int) ((Math.random() * (max - min + 1)) + min);
	}


	/**
	 * Returns a trimmed string, with appended "..."
	 * @param string: input string
	 * @param maxLength: max length of output string
	 * @return String
	 */
	public static String clampString(String string, int maxLength) {
		if (string.length() > maxLength) {
			return string.substring(0, maxLength - 3) + "...";
		} else {
			return string;
		}
	}


	public static String getBitRateInKBPS(String path) {
		StringBuilder result = new StringBuilder();
		MediaExtractor mediaExtractor = new MediaExtractor();
		try {
			mediaExtractor.setDataSource(path);
		} catch (Exception e) {
			e.printStackTrace();
		}

		MediaFormat mediaFormat = mediaExtractor.getTrackFormat(0);
		if (mediaFormat.containsKey(MediaFormat.KEY_BIT_RATE)) {
			int bitRate = mediaFormat.getInteger(MediaFormat.KEY_BIT_RATE) / 1000;
			result.append(bitRate).append(" kbps");
		} else {
			if (mediaFormat.containsKey("max-bitrate")) {
				int bitRate = mediaFormat.getInteger("max-bitrate") / 1000;
				result.append("dynamic (").append(bitRate).append("  kbps max)");
			} else {
				result.append("<failed>");
			}
		}
		if (mediaFormat.containsKey(MediaFormat.KEY_SAMPLE_RATE)) {
			result.append(" @ ").append(mediaFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE) / 1000).append(" KHz");
		}
		return result.toString();
	}

	/**
	 * Returns the album art of an audio file, if existent
	 *
	 * @param path: file path
	 * @return Bitmap || null
	 */
	public static Bitmap extractAlbumArt(String path) {
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();
		mmr.setDataSource(path);

		byte[] bitmapData = mmr.getEmbeddedPicture();
		mmr.release();
		if (bitmapData != null) {
			return BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length);
		}
		return null;
	}

}
