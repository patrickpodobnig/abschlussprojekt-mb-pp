package com.finalproject.simplay.tabfragment;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.finalproject.simplay.R;
import com.finalproject.simplay.adapter.PlaylistSongsRecyclerViewAdapter;
import com.finalproject.simplay.callback.PlaylistMoveCallback;
import com.finalproject.simplay.model.AudioFile;
import com.finalproject.simplay.repository.DatabaseHelper;
import com.finalproject.simplay.util.Helper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.turingtechnologies.materialscrollbar.AlphabetIndicator;
import com.turingtechnologies.materialscrollbar.DragScrollBar;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

//TODO Back Button

public class PlaylistSongsFragment extends Fragment {

	private AtomicBoolean areFabsVisible = new AtomicBoolean(false);
	private FloatingActionButton fab, fabDelete, fabAdd;
	private Animation fabOpen, fabClose;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_playlist_songs, container, false);
		RecyclerView recyclerView = view.findViewById(R.id.playlist_song_recycler_view);
		fab = view.findViewById(R.id.playlist_song_fab);
		fabDelete = view.findViewById(R.id.playlist_delete_song_fab);
		fabAdd = view.findViewById(R.id.playlist_add_song_fab);

		fabOpen = AnimationUtils.loadAnimation(this.getContext(), R.anim.fab_open);
		fabClose = AnimationUtils.loadAnimation(this.getContext(), R.anim.fab_close);

		//Receiving String from Bundle
		Bundle args = this.getArguments();
		assert args != null;
		String tableName = args.getString("playlistName");

		//Getting lost of specified playlistTable
		DatabaseHelper databaseHelper = DatabaseHelper.getInstance(getContext());
		ArrayList<AudioFile> musicList = databaseHelper.getPlaylist(tableName);
		String playlistName = databaseHelper.getPlaylistName(tableName);
		databaseHelper.close();

		//Setting up Adapter with musicList
		PlaylistSongsRecyclerViewAdapter adapter = new PlaylistSongsRecyclerViewAdapter(tableName);
		adapter.setPlaylistSongs(musicList);
		recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

		ItemTouchHelper.Callback callback = new PlaylistMoveCallback(adapter);
		ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
		touchHelper.attachToRecyclerView(recyclerView);

		recyclerView.setAdapter(adapter);

		TextView titleBarText = view.findViewById(R.id.title_bar_text);
		titleBarText.setText(playlistName);
		titleBarText.setSingleLine(true);
		AppCompatImageButton btnNavBack = view.findViewById(R.id.btn_nav_back);
		btnNavBack.setOnClickListener(btnNavBackClicked -> Navigation.findNavController(btnNavBackClicked).popBackStack());

		//Onclick Listener to navigate to song selection
		fab.setOnClickListener(fabClicked -> toggleFabs());

		fabAdd.setOnClickListener(fabAddClicked -> {
			Bundle bundle = new Bundle();
			bundle.putString("playlistName", tableName);
			bundle.putString("intention", "add");
			areFabsVisible.set(false);
			Navigation.findNavController(fabAddClicked).navigate(R.id.action_playlistSongs_to_songSelectionFragment, bundle);
		});

		fabDelete.setOnClickListener(fabDeleteClicked -> {
			Bundle bundle = new Bundle();
			bundle.putString("playlistName", tableName);
			bundle.putString("intention", "delete");
			areFabsVisible.set(false);
			Navigation.findNavController(fabDeleteClicked).navigate(R.id.action_playlistSongs_to_songSelectionFragment, bundle);
		});

		DragScrollBar scrollBar = view.findViewById(R.id.scrollBar);
		scrollBar.setRecyclerView(recyclerView);
		//scrollBar.setIndicator(new AlphabetIndicator(view.getContext()), false);

		return view;
	}

	private void toggleFabs() {
		if (areFabsVisible.get()) {
			fabAdd.setAnimation(fabClose);
			fabAdd.hide();
			fabDelete.setAnimation(fabClose);
			fabDelete.hide();
			fab.setImageResource(R.drawable.ic_edit_playlist_dot);
			fab.animate().rotation(0);
		} else {
			fabAdd.show();
			fabAdd.setAnimation(fabOpen);
			fabDelete.show();
			fabDelete.setAnimation(fabOpen);
			fab.setImageResource(R.drawable.ic_close);
			fab.animate().rotation(720);
		}
		areFabsVisible.set(!areFabsVisible.get());
	}
}