package com.finalproject.simplay.adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.simplay.R;
import com.finalproject.simplay.model.Genre;

import java.util.ArrayList;

public class GenreRecyclerViewAdapter extends RecyclerView.Adapter<GenreRecyclerViewAdapter.GenreViewHolder> {

	private final ArrayList<Genre> genreList;

	public GenreRecyclerViewAdapter(ArrayList<Genre> genreList) {
		this.genreList = genreList;
	}

	@NonNull
	@Override
	public GenreViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_generic, parent, false);
		return new GenreViewHolder(view);

	}

	@Override
	public void onBindViewHolder(@NonNull GenreViewHolder holder, int position) {
		holder.genreName.setText(genreList.get(position).getName());

		holder.itemView.setOnClickListener(view -> {
			Bundle bundle = new Bundle();
			bundle.putString("filterString", genreList.get(position).getName());
			bundle.putString("origin", "Genre");
			bundle.putLong("genreId", genreList.get(position).getId());
			Navigation.findNavController(view).navigate(R.id.action_viewPagerFragmentContainer_to_specificSongs, bundle);

		});
	}

	@Override
	public int getItemCount() {
		return genreList.size();
	}

	public class GenreViewHolder extends RecyclerView.ViewHolder {
		private final TextView genreName;

		public GenreViewHolder(@NonNull View itemView) {
			super(itemView);
			genreName = itemView.findViewById(R.id.txtGenericList);
		}

	}
}
