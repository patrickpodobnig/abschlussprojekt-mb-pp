package com.finalproject.simplay.model;

public class Bookmark {

    private int bookmarkId, songId, position;
    private String title;

    public Bookmark(int songId, int position, String title) {
        this.songId = songId;
        this.position = position;
        this.title = title;
    }

    public Bookmark(int bookmarkId, int songId, int position, String title) {
        this.bookmarkId = bookmarkId;
        this.songId = songId;
        this.position = position;
        this.title = title;
    }

    public int getBookmarkId() {
        return bookmarkId;
    }

    public int getSongId() {
        return songId;
    }

    public int getPosition() {
        return position;
    }

    public String getTitle() {
        return title;
    }
}
