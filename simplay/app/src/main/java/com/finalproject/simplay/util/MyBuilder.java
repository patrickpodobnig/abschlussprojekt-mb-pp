package com.finalproject.simplay.util;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.view.ContextThemeWrapper;
import androidx.navigation.Navigation;

import com.finalproject.simplay.R;
import com.finalproject.simplay.repository.DatabaseHelper;
import com.finalproject.simplay.model.AudioFile;
import com.finalproject.simplay.model.Playlist;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class MyBuilder {

	public static final String TOAST_SYSTEM = "TOAST_SYSTEM";
	public static final String TOAST_NOTIFICATION = "TOAST_NOTIFICATION";

	// https://stackoverflow.com/questions/31641973/how-to-blur-background-images-in-android
	private static final float BITMAP_SCALE = 0.8f;
	private static final float BLUR_RADIUS = 17.5f;

	public static Bitmap blur(Context context, Bitmap image) {
		int width = Math.round(image.getWidth() * BITMAP_SCALE);
		int height = Math.round(image.getHeight() * BITMAP_SCALE);

		Bitmap inputBitmap = Bitmap.createScaledBitmap(image, width, height, false);
		Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);

		RenderScript rs = RenderScript.create(context);
		ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
		Allocation tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
		Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
		theIntrinsic.setRadius(BLUR_RADIUS);
		theIntrinsic.setInput(tmpIn);
		theIntrinsic.forEach(tmpOut);
		tmpOut.copyTo(outputBitmap);

		rs.destroy();
		theIntrinsic.destroy();
		tmpIn.destroy();
		tmpOut.destroy();
		return outputBitmap;
	}


	public static Toast createToast(Context context, String type, String message) {
		int resourceId;
		switch (type) {
			case TOAST_SYSTEM:
				resourceId = R.layout.toast_system_message;
				break;
			case TOAST_NOTIFICATION:
			default:
				resourceId = R.layout.toast_notification;
				break;
		}

		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View toastView = inflater.inflate(resourceId, null);
		Toast toast = Toast.makeText(context, "", Toast.LENGTH_SHORT);
		TextView messageView = toastView.findViewById(R.id.toast_text);
		messageView.setText(message);
		toast.setView(toastView);
		return toast;
	}


	public static PopupMenu createGenericPopup(Context itemViewContext, View imageButton) {
		Context wrapper = new ContextThemeWrapper(itemViewContext, R.style.Theme_Simplay_PopupMenu);
		return new PopupMenu(wrapper, imageButton);
	}


	public static AlertDialog.Builder createGenericDialogBuilder(Context context) {
		Context wrapper = new ContextThemeWrapper(context, R.style.Theme_Simplay_AlertDialog);
		return new AlertDialog.Builder(wrapper);
	}


	public static AlertDialog createAddToPlaylistDialog(View itemView, AudioFile addedTrack, String origin) {
		DatabaseHelper databaseHelper = DatabaseHelper.getInstance(itemView.getContext());
		Context wrapper = new ContextThemeWrapper(itemView.getContext(), R.style.Theme_Simplay_AlertDialog);
		AlertDialog.Builder builder = new AlertDialog.Builder(wrapper);
		builder.setTitle("To which Playlist do you want to add\n" + Formatter.prepareDblQuotedString(addedTrack.getTitle(), true));

		ArrayList<Playlist> playlists = databaseHelper.getPlayLists();
		int totalPlaylists = playlists.size();
		String[] dialogPlaylists = new String[totalPlaylists];
		for (int i = 0; i < totalPlaylists; i++) {
			dialogPlaylists[i] = playlists.get(i).getPlaylistTitle();
		}

		builder.setItems(dialogPlaylists, (dialogInterface, i) -> {
			Playlist targetPlaylist = playlists.get(i);
			databaseHelper.addToPlaylist(targetPlaylist.getPlaylistTableName(), addedTrack);

			if (origin.equals("CurrentSongs") || origin.equals("PlaylistSongs")) {
				MyBuilder.createToast(itemView.getContext(), TOAST_SYSTEM, "Successfully added song to playlist.").show();

			} else {
				// Not working except when being invoked from within AllSongsRecViewAdapter
				// E: Navigation action/destination com.finalproject.simplay:id/action_viewPagerFragmentContainer_to_playlistSongs cannot be found from the current destination Destination(com.finalproject.simplay:id/specificSongs) label=fragment_specific_songs class=com.finalproject.simplay.tabfragment.SpecificSongsFragment


				final Snackbar snackbar = Snackbar.make(itemView, "", BaseTransientBottomBar.LENGTH_LONG);
				LayoutInflater inflater = (LayoutInflater) itemView.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View snackBarView = inflater.inflate(R.layout.snackbar_generic, null);
				TextView messageView = snackBarView.findViewById(R.id.snackbar_text);
				messageView.setText("Click to navigate to " + Formatter.prepareDblQuotedString(targetPlaylist.getPlaylistTitle()));

				Snackbar.SnackbarLayout snackBarLayout = (Snackbar.SnackbarLayout) snackbar.getView();
				snackBarLayout.setBackgroundColor(itemView.getResources().getColor(R.color.white_dialog));

				snackBarLayout.addView(snackBarView);
				snackbar.setAction("Confirm", snackBackClicked -> {

//				Snackbar.make(itemView, "Click to navigate to " + Formatter.prepareDblQuotedString(targetPlaylist.getPlaylistTitle(), true), BaseTransientBottomBar.LENGTH_LONG)
//						.setAction("Confirm", snackbackClicked -> {

					Bundle bundle = new Bundle();
					bundle.putString("playlistName", targetPlaylist.getPlaylistTableName());

					switch (origin) {
						case "AllSongs":
							Navigation.findNavController(itemView).navigate(R.id.action_viewPagerFragmentContainer_to_playlistSongs, bundle);
							break;
//						//crashes
//						case "PlaylistSongs":
//							Navigation.findNavController(itemView).navigate(R.id.action_playlistSongs_self, bundle);
//							break;
						case "SpecificSongs":
							Navigation.findNavController(itemView).navigate(R.id.action_specificSongs_to_playlistSongs, bundle);
							break;
					}
				}).show();

			}

		});

		return builder.create();
	}

}
