package com.finalproject.simplay.tabfragment;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.finalproject.simplay.R;
import com.finalproject.simplay.adapter.ArtistRecyclerViewAdapter;
import com.finalproject.simplay.repository.MediaStoreHelper;
import com.finalproject.simplay.util.Helper;
import com.turingtechnologies.materialscrollbar.AlphabetIndicator;
import com.turingtechnologies.materialscrollbar.DragScrollBar;
import com.turingtechnologies.materialscrollbar.TouchScrollBar;

import java.util.ArrayList;


public class ArtistFragment extends Fragment {

	private RecyclerView recyclerView;
	private SearchView searchView;
	private ArtistRecyclerViewAdapter adapter;
	private ArrayList<String> artists;


	@Override
	public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		MenuItem menuItem = menu.findItem(R.id.action_search);
		menuItem.setVisible(true);

		searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {
				return false;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				ArrayList<String> tempArtistList = new ArrayList<>();
				if (newText.equals("")) {
					adapter.setArtistList(artists);
				} else {
					for (String artist : artists) {
						if (artist.toUpperCase().contains(newText.toUpperCase())) {
							tempArtistList.add(artist);
						}
					}
					adapter.setArtistList(tempArtistList);
				}
				recyclerView.setAdapter(adapter);

				return false;
			}
		});
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_artist, container, false);
		setHasOptionsMenu(true);
		//initialising RecyclerView and respective ArrayList
		recyclerView = view.findViewById(R.id.artist_recycler_view);
		artists = new ArrayList<>();

		MediaStoreHelper mediaStoreHelper = new MediaStoreHelper(requireActivity().getApplicationContext());
		artists = mediaStoreHelper.getArtists();

		//setting up the Adapter and LayoutManager
		adapter = new ArtistRecyclerViewAdapter();
		adapter.setArtistList(artists);
		recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
		recyclerView.setAdapter(adapter);

		DragScrollBar scrollBar = view.findViewById(R.id.scrollBar);
		scrollBar.setRecyclerView(recyclerView);
		scrollBar.setIndicator(new AlphabetIndicator(view.getContext()), false);

		return view;
	}

}