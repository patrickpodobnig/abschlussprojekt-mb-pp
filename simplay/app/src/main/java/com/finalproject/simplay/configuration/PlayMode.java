package com.finalproject.simplay.configuration;

public class PlayMode {

	public static final String MODE_SHUFFLE = "shuffle";
	public static final String MODE_REPEAT = "repeat";

	public static final String MODE_INACTIVE = "inactive";
	public static final String MODE_ACTIVE = "active";

	public static final String REPEAT_ALL = "repeat_all";
	public static final String REPEAT_ONE = "repeat_one";

	public static String print(String playMode) {
		switch (playMode) {
			case MODE_ACTIVE:
				return "ON";
			case MODE_INACTIVE:
				return "OFF";
			case REPEAT_ALL:
				return "ALL";
			case REPEAT_ONE:
				return "ONE";
			default:
				return null;
		}
	}

}
