package com.finalproject.simplay.tabfragment;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.finalproject.simplay.R;
import com.finalproject.simplay.adapter.GenreRecyclerViewAdapter;
import com.finalproject.simplay.model.Genre;
import com.finalproject.simplay.repository.MediaStoreHelper;
import com.finalproject.simplay.util.Helper;

import java.util.ArrayList;


public class GenreFragment extends Fragment {

	RecyclerView recyclerView;

	public GenreFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_genre, container, false);

		recyclerView = view.findViewById(R.id.genre_recycler_view);

		MediaStoreHelper mediaStoreHelper = new MediaStoreHelper(getContext().getApplicationContext());
		ArrayList<Genre> genreList = mediaStoreHelper.getGenres();

		GenreRecyclerViewAdapter adapter = new GenreRecyclerViewAdapter(genreList);
		recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
		recyclerView.setAdapter(adapter);

		return view;
	}

}