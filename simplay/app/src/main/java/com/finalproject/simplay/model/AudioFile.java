package com.finalproject.simplay.model;

import java.io.Serializable;

public class AudioFile implements Serializable {

    private static final long serialVersionUID = 1L;

    // Needed when handling playlists (adding/removing songs)
    private int dbIndex;
    // We need this in order to prevent RecyclerViewAdapter from recycling checked checkboxes when scrolling down a list.
    private boolean isSelected;

    private int id;
    private String artist;
    private String title;
    private int duration;
    private String dataPath;

    public AudioFile(int id, String artist, String title, int duration, String dataPath) {
        this.id = id;
        this.artist = artist;
        this.title = title;
        this.duration = duration;
        this.dataPath = dataPath;
    }

    // Overload to additionally set the database index, for handling playlists
    public AudioFile(int dbIndex, int id, String artist, String title, int duration, String dataPath) {
        this.dbIndex = dbIndex;
        this.id = id;
        this.artist = artist;
        this.title = title;
        this.duration = duration;
        this.dataPath = dataPath;
    }

    public int getDbIndex() {
        return dbIndex;
    }

    public void setDbIndex(int dbIndex) {
        this.dbIndex = dbIndex;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getDataPath() {
        return dataPath;
    }

    public void setDataPath(String dataPath) {
        this.dataPath = dataPath;
    }
}
