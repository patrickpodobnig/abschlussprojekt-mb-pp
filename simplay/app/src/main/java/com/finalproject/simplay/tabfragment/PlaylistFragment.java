package com.finalproject.simplay.tabfragment;

import android.app.AlertDialog;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.simplay.R;
import com.finalproject.simplay.adapter.PlaylistRecyclerViewAdapter;
import com.finalproject.simplay.model.Playlist;
import com.finalproject.simplay.repository.DatabaseHelper;
import com.finalproject.simplay.util.MyBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;


public class PlaylistFragment extends Fragment {
    DatabaseHelper databaseHelper;
    ArrayList<Playlist> playlists;
    PlaylistRecyclerViewAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_playlist, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.playlist_recycler_view);
        FloatingActionButton fab = view.findViewById(R.id.playlist_fab);


        //Initialising databaseHelper to get all playlist names
        databaseHelper = DatabaseHelper.getInstance(getContext());
        playlists = databaseHelper.getPlayLists();

        //Initialising Adapter for RecyclerView and passing list
        adapter = new PlaylistRecyclerViewAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        adapter.setPlaylists(playlists);
        adapter.setContext(requireActivity().getBaseContext());
        recyclerView.setAdapter(adapter);

        //Setting OnClickListener on Floating Action Button
        fab.setOnClickListener(fabClicked -> {

            //Initialising PopUp to input PlaylistName
            AlertDialog.Builder builder = MyBuilder.createGenericDialogBuilder(getContext());
            builder.setCancelable(false);
            builder.setTitle("Enter the name of your new playlist");

            final EditText inputView = new EditText(getContext());
            inputView.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(inputView);

            //Setting OnClickListeners to negative and positive Response Buttons
            builder.setPositiveButton("Create Playlist", (dialog, which) -> {
                String inputText = inputView.getText().toString().trim();
                if (!inputText.equals("")) {
                    databaseHelper.createNewPlaylist(inputText);
                    adapter.setPlaylists(databaseHelper.getPlayLists());
                    dialog.dismiss();
                } else {
                    MyBuilder.createToast(getContext(), MyBuilder.TOAST_NOTIFICATION, "Please enter a name.").show();
                }
            });
            builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
            builder.show();
        });

        return view;
    }


}