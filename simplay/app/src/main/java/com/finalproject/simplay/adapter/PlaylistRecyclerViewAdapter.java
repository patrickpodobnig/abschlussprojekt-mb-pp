package com.finalproject.simplay.adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.finalproject.simplay.R;
import com.finalproject.simplay.model.Playlist;
import com.finalproject.simplay.repository.DatabaseHelper;
import com.finalproject.simplay.util.Helper;
import com.finalproject.simplay.util.MyBuilder;
import com.finalproject.simplay.util.Formatter;

import java.util.ArrayList;

public class PlaylistRecyclerViewAdapter extends RecyclerView.Adapter<PlaylistRecyclerViewAdapter.PlaylistViewHolder> {
    private ArrayList<Playlist> playlists;
    // TODO: Investigate passed context, dialogs suffer from inconsistencies depending
    //  on usage of context or holder.itemView.getContext() < second displays it correctly
    private Context context;

    public void setContext(Context context) {
        this.context = context;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setPlaylists(ArrayList<Playlist> playlists) {
        this.playlists = playlists;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PlaylistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_generic_with_kebab_menu, parent, false);
        return new PlaylistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PlaylistViewHolder holder, @SuppressLint("RecyclerView") int position) {
        Playlist playlist = playlists.get(position);

        holder.playListName.setText(playlist.getPlaylistTitle());
        holder.playListName.setSingleLine(true);

        holder.kebapMenuButton.setOnClickListener(kebapMenuButtonClicked -> {
                AlertDialog.Builder builder = MyBuilder.createGenericDialogBuilder(holder.itemView.getContext());
                PopupMenu popupMenu = MyBuilder.createGenericPopup(holder.itemView.getContext(), holder.kebapMenuButton);
                popupMenu.getMenuInflater().inflate(R.menu.menu_dropdown, popupMenu.getMenu());
                popupMenu.getMenu().add("Delete");
                popupMenu.getMenu().add("Rename");
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    final DatabaseHelper databaseHelper = DatabaseHelper.getInstance(context);

                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getTitle().equals("Delete")) {
                            builder.setCancelable(false);
                            builder.setTitle("Are you sure you want to delete\n" + Formatter.prepareDblQuotedString(playlist.getPlaylistTitle(), true) + " ?");
                            builder.setPositiveButton("Delete", (dialogInterface, i) -> {
                                databaseHelper.deletePlaylist(playlist.getPlaylistTableName());
                                ArrayList<Playlist> updatedPlaylistList = databaseHelper.getPlayLists();
                                setPlaylists(updatedPlaylistList);
                            });
                            builder.setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.dismiss());
                            builder.show();
                        } else {
                            builder.setCancelable(false);
                            builder.setTitle("Please enter the new name");
                            final EditText inputView = new EditText(holder.itemView.getContext());
                            inputView.setInputType(InputType.TYPE_CLASS_TEXT);
                            inputView.setText(playlist.getPlaylistTitle());
                            inputView.setSelectAllOnFocus(true);
                            inputView.requestFocus();
                            builder.setView(inputView);
                            builder.setPositiveButton("Rename", (dialogInterface, i) -> {
                                String inputText = inputView.getText().toString().trim();
                                if (inputText.equals("")) {
                                    dialogInterface.dismiss();
                                    MyBuilder.createToast(context, MyBuilder.TOAST_NOTIFICATION, "Playlist not renamed.").show();
                                } else {
                                    databaseHelper.renamePlaylist(playlist.getPlaylistTableName(), inputText);
                                    ArrayList<Playlist> updatedPlaylistList = databaseHelper.getPlayLists();
                                    setPlaylists(updatedPlaylistList);
                                }
                            });
                            builder.setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.dismiss());
                            builder.show();
                        }
                        return true;
                    }
                });
                popupMenu.show();
            });

        holder.itemView.setOnClickListener(view -> {
            if (!playlist.getPlaylistTableName().equals("")) {
                Bundle bundle = new Bundle();
                bundle.putString("playlistName", playlist.getPlaylistTableName());
                Navigation.findNavController(view).navigate(R.id.action_viewPagerFragmentContainer_to_playlistSongs, bundle);
            }
        });
    }

    @Override
    public int getItemCount() {
        return playlists.size();
    }

    public static class PlaylistViewHolder extends RecyclerView.ViewHolder {
        private final TextView playListName;
        private final AppCompatImageButton kebapMenuButton;

        public PlaylistViewHolder(@NonNull View itemView) {
            super(itemView);
            playListName = itemView.findViewById(R.id.txtPlaylist);
            kebapMenuButton = itemView.findViewById(R.id.playlist_spinner_toggle);
        }
    }
}
